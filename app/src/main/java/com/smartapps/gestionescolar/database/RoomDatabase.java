package com.smartapps.gestionescolar.database;

/*
 * Copyright (C) 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.smartapps.gestionescolar.models.courseyear.CourseYear;
import com.smartapps.gestionescolar.models.courseyear.CourseYearDao;
import com.smartapps.gestionescolar.models.division.Division;
import com.smartapps.gestionescolar.models.division.DivisionDao;
import com.smartapps.gestionescolar.models.documentations.Documentation;
import com.smartapps.gestionescolar.models.documentations.DocumentationDao;
import com.smartapps.gestionescolar.models.employeejobtitles.EmployeeJobTitle;
import com.smartapps.gestionescolar.models.employeejobtitles.EmployeeJobTitleDao;
import com.smartapps.gestionescolar.models.people.Person;
import com.smartapps.gestionescolar.models.people.PersonDao;
import com.smartapps.gestionescolar.models.persongenders.PersonGenders;
import com.smartapps.gestionescolar.models.persongenders.PersonGendersDao;
import com.smartapps.gestionescolar.models.relationship.Relationship;
import com.smartapps.gestionescolar.models.relationship.RelationshipDao;
import com.smartapps.gestionescolar.models.shifts.Shift;
import com.smartapps.gestionescolar.models.shifts.ShiftDao;
import com.smartapps.gestionescolar.models.shifts.ShiftRepository;
import com.smartapps.gestionescolar.models.studentdivision.StudentDivisions;
import com.smartapps.gestionescolar.models.studentdivision.StudentDivisionsDao;
import com.smartapps.gestionescolar.models.studentdocumentations.StudentDocumentation;
import com.smartapps.gestionescolar.models.studentdocumentations.StudentDocumentationDao;
import com.smartapps.gestionescolar.models.students.Student;
import com.smartapps.gestionescolar.models.students.StudentDao;
import com.smartapps.gestionescolar.models.teacher.Teacher;
import com.smartapps.gestionescolar.models.teacher.TeacherDao;
import com.smartapps.gestionescolar.models.tutor.Tutor;
import com.smartapps.gestionescolar.models.tutor.TutorDao;
import com.smartapps.gestionescolar.models.tutorStudents.TutorStudents;
import com.smartapps.gestionescolar.models.tutorStudents.TutorStudentsDao;
import com.smartapps.gestionescolar.models.typeemployee.TypeEmployee;
import com.smartapps.gestionescolar.models.typeemployee.TypeEmployeeDao;
import com.smartapps.gestionescolar.ui.login.LoginActivity;

/**
 * This is the backend. The database. This used to be done by the OpenHelper.
 * The fact that this has very few comments emphasizes its coolness.
 */

@Database(entities = {
        Division.class,
        Person.class,
        Student.class,
        CourseYear.class,
        Relationship.class,
        StudentDivisions.class,
        Tutor.class,
        TutorStudents.class,
        PersonGenders.class,
        Teacher.class,
        Shift.class,
        EmployeeJobTitle.class,
        TypeEmployee.class,
        Documentation.class,
        StudentDocumentation.class
}, version = 1)
@TypeConverters({Converters.class})
public abstract class RoomDatabase extends android.arch.persistence.room.RoomDatabase {

    public abstract StudentDao studentDao();

    public abstract PersonDao personDao();

    public abstract CourseYearDao courseYearDao();

    public abstract DivisionDao divisionDao();

    public abstract StudentDivisionsDao studentDivisionsDao();

    public abstract RelationshipDao relationshipDao();

    public abstract PersonGendersDao personGendersDao();

    public abstract TutorStudentsDao tutorStudentsDao();

    public abstract TutorDao tutorDao();

    public abstract TeacherDao teacherDao();

    public abstract ShiftDao shiftDao();

    public abstract EmployeeJobTitleDao employeeJobTitleDao();

    public abstract TypeEmployeeDao typeEmployeeDao();

    public abstract DocumentationDao documentationDao();

    public abstract StudentDocumentationDao studentDocumentationDao();

    private static RoomDatabase INSTANCE;

    public static RoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (RoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            RoomDatabase.class, "SiGE")
                            // Wipes and rebuilds instead of migrating if no Migration object.
                            // Migration is not part of this codelab.
                            .fallbackToDestructiveMigration()
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Override the onOpen method to populate the database.
     * For this sample, we clear the database every time it is created or opened.
     */
    private static android.arch.persistence.room.RoomDatabase.Callback sRoomDatabaseCallback = new android.arch.persistence.room.RoomDatabase.Callback() {

        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
        }

        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
        }
    };
}
