package com.smartapps.gestionescolar.dependencyinjection;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.smartapps.gestionescolar.database.RoomDatabase;

import dagger.Module;

@Module
public class RoomModule {
    private final RoomDatabase database;

    public RoomModule(Application application) {
        this.database = Room.databaseBuilder(
                application,
                RoomDatabase.class,
                "ListItem.db"
        ).build();
    }

//    @Provides
//    @Singleton
//    StudentRepository provideStudentRepository(StudentDao listItemDao){
//        return new StudentRepository(listItemDao);
//    }
//
//    @Provides
//    @Singleton
//    RoomDatabase provideWordRoomDatabase(Application application){
//        return database;
//    }
}
