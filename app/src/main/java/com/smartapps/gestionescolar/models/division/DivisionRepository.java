package com.smartapps.gestionescolar.models.division;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.smartapps.gestionescolar.database.RoomDatabase;
import com.smartapps.gestionescolar.services.DownloadService;
import com.smartapps.gestionescolar.util.DateDeserializer;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;

public class DivisionRepository {
    private DivisionDao divisionDao;

    DivisionRepository(Application application) {
        RoomDatabase db = RoomDatabase.getDatabase(application);
        divisionDao = db.divisionDao();
    }

    public void insert(Division person) {
        divisionDao.insert(person);
    }

    public LiveData<List<Division>> getAll() {
        return divisionDao.getAll();
    }

    public Observable<JSONObject> rxDownload() {
        return new DownloadService().rxDownload("api/divisions/all");
    }

    public void save(JSONObject courseYears) {
        try {
            divisionDao.deleteAll();

            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
            Gson gson = gsonBuilder.create();

            JSONArray response = courseYears.getJSONArray("Result");
            if (response.length() > 0) {
                ArrayList<Division> items = new ArrayList<>();
                for (int i = 0; i < response.length(); i++) {
                    Division item = gson.fromJson(response.get(i).toString(), Division.class);
                    divisionDao.insert(item);
                }
            }
        } catch (Exception e) {
            Log.e("-", e.getMessage());
        }
    }
}
