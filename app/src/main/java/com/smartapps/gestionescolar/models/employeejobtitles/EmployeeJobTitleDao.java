package com.smartapps.gestionescolar.models.employeejobtitles;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.smartapps.gestionescolar.models.base.BaseDao;

import java.util.List;

@Dao
public abstract class EmployeeJobTitleDao implements BaseDao<EmployeeJobTitle> {
    @Query("select * from EmployeeJobTitles where Id=:id")
    abstract EmployeeJobTitle getById(int id);

    @Query("select * from EmployeeJobTitles where EmployeeId=:employeeId and ShiftId=:shiftId")
    abstract EmployeeJobTitle get(String employeeId, int shiftId);

    @Insert
    abstract void insertAll(List<EmployeeJobTitle> persons);

    @Query("select * from EmployeeJobTitles")
    abstract LiveData<List<EmployeeJobTitle>> getAll();
}