package com.smartapps.gestionescolar.models.students;

import android.app.Application;
import android.arch.lifecycle.LiveData;

import com.smartapps.gestionescolar.database.RoomDatabase;
import com.smartapps.gestionescolar.models.students.pojo.ImageToSync;
import com.smartapps.gestionescolar.models.students.pojo.StudentData;
import com.smartapps.gestionescolar.models.students.pojo.StudentDataSync;
import com.smartapps.gestionescolar.models.students.pojo.StudentListData;

import java.util.List;

import io.reactivex.Maybe;

public class StudentRepository {
    private StudentDao studentDao;

    // Note that in order to unit test the WordRepository, you have to remove the Application
    // dependency. This adds complexity and much more code, and this sample is not about testing.
    // See the BasicSample in the android-architecture-components repository at
    // https://github.com/googlesamples
    StudentRepository(Application application) {
        RoomDatabase db = RoomDatabase.getDatabase(application);
        studentDao = db.studentDao();

    }

    public void insert(Student student) {
        studentDao.insert(student);
    }

    public Maybe<StudentData> getStudent(String id) {
        return studentDao.getStudent(id);
    }

    public Student getStudentById(String id) {
        return studentDao.getStudentById(id);
    }

    public LiveData<List<StudentListData>> getStudentList(int courseYearId, int shiftId) {
        return studentDao.getStudentList(courseYearId, shiftId);
    }

    public void update(Student student) {
        studentDao.update(student);
    }

    List<StudentDataSync> getStudentToSync() {
        return studentDao.getStudentToSync();
    }

    List<ImageToSync> getCardFrontImageToSync() {
        return studentDao.getCardFrontImageToSync();
    }

    List<ImageToSync> getCardBackImageToSync() {
        return studentDao.getCardBackImageToSync();
    }
}
