package com.smartapps.gestionescolar.models.division;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.smartapps.gestionescolar.models.base.BaseDao;

import java.util.List;


@Dao
public abstract class DivisionDao implements BaseDao<Division> {

    @Query("select * from Divisions")
    public abstract LiveData<List<Division>> getAll();

    @Query("delete from Divisions")
    public abstract void deleteAll();
}
