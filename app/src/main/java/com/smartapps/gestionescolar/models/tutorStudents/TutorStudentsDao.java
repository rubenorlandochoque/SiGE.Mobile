package com.smartapps.gestionescolar.models.tutorStudents;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.smartapps.gestionescolar.models.base.BaseDao;

import java.util.List;


@Dao
public abstract class TutorStudentsDao implements BaseDao<TutorStudents> {

    @Query("select * from TutorStudents")
    public abstract LiveData<List<TutorStudents>> getAll();

    @Query("delete from TutorStudents")
    public abstract void deleteAll();

    @Query("update TutorStudents set CurrentTutor = 0 where StudentId = :studentId")
    public abstract void disableAll(String studentId);
}
