package com.smartapps.gestionescolar.models.students;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.smartapps.gestionescolar.models.people.Person;

@Entity(tableName = "Students", foreignKeys = {
        @ForeignKey(entity = Person.class,
                parentColumns = "Id",
                childColumns = "Id"
        )
})
public class Student {
    @PrimaryKey
    @NonNull
    private String Id;
    private boolean HasBrothersOfSchoolAge;
    private String IdCardFront;
    private String IdCardBack;
    private boolean Sync = false;
    private boolean SyncCardFront = false;
    private boolean SyncCardBack = false;
    private String Observation;
    private String SchoolOrigin;
    private String OtherPhoneBelongs;

    public String getOtherPhoneBelongs() {
        return OtherPhoneBelongs;
    }

    public void setOtherPhoneBelongs(String otherPhoneBelongs) {
        OtherPhoneBelongs = otherPhoneBelongs;
    }

    public String getObservation() {
        return Observation;
    }

    public void setObservation(String observation) {
        Observation = observation;
    }

    public String getSchoolOrigin() {
        return SchoolOrigin;
    }

    public void setSchoolOrigin(String schoolOrigin) {
        SchoolOrigin = schoolOrigin;
    }

    public boolean isSyncCardFront() {
        return SyncCardFront;
    }

    public void setSyncCardFront(boolean syncCardFront) {
        SyncCardFront = syncCardFront;
    }

    public boolean isSyncCardBack() {
        return SyncCardBack;
    }

    public void setSyncCardBack(boolean syncCardBack) {
        SyncCardBack = syncCardBack;
    }

    public boolean isSync() {
        return Sync;
    }

    public void setSync(boolean sync) {
        Sync = sync;
    }

    public String getIdCardFront() {
        return IdCardFront;
    }

    public void setIdCardFront(String idCardFront) {
        IdCardFront = idCardFront;
    }

    public String getIdCardBack() {
        return IdCardBack;
    }

    public void setIdCardBack(String idCardBack) {
        IdCardBack = idCardBack;
    }

    @NonNull
    public String getId() {
        return Id;
    }

    public void setId(@NonNull String id) {
        this.Id = id;
    }

    public boolean isHasBrothersOfSchoolAge() {
        return HasBrothersOfSchoolAge;
    }

    public void setHasBrothersOfSchoolAge(boolean hasBrothersOfSchoolAge) {
        this.HasBrothersOfSchoolAge = hasBrothersOfSchoolAge;
    }
}
