package com.smartapps.gestionescolar.models.typeemployee;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "TypeEmployees")
public class TypeEmployee {
    @PrimaryKey
    @NonNull
    private int Id;
    private String Description;

    @NonNull
    public int getId() {
        return Id;
    }

    public void setId(@NonNull int id) {
        Id = id;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}
