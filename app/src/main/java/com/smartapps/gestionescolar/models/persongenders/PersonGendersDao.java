package com.smartapps.gestionescolar.models.persongenders;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.smartapps.gestionescolar.models.base.BaseDao;

import java.util.List;


@Dao
public abstract class PersonGendersDao implements BaseDao<PersonGenders> {

    @Query("select * from PersonGenders")
    public abstract LiveData<List<PersonGenders>> getAll();

    @Query("delete from PersonGenders")
    public abstract void deleteAll();
}
