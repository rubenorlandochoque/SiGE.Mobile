package com.smartapps.gestionescolar.models.relationship;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.smartapps.gestionescolar.models.base.BaseDao;
import com.smartapps.gestionescolar.models.relationship.pojo.RelationshipData;

import java.util.List;


@Dao
public abstract class RelationshipDao implements BaseDao<Relationship> {

    @Query("select Id,Description from Relationships")
    abstract LiveData<List<RelationshipData>> getAll();

    @Query("delete from Relationships")
    public abstract void deleteAll();
}
