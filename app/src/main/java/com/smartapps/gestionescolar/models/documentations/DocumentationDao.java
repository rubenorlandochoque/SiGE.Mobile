package com.smartapps.gestionescolar.models.documentations;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.smartapps.gestionescolar.models.base.BaseDao;
import com.smartapps.gestionescolar.models.persongenders.PersonGenders;

import java.util.List;


@Dao
public abstract class DocumentationDao implements BaseDao<Documentation> {

    @Query("select * from Documentations")
    public abstract LiveData<List<Documentation>> getAll();

    @Query("delete from Documentations")
    public abstract void deleteAll();
}
