package com.smartapps.gestionescolar.models.tutorStudents;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

import com.smartapps.gestionescolar.models.relationship.Relationship;
import com.smartapps.gestionescolar.models.students.Student;
import com.smartapps.gestionescolar.models.tutor.Tutor;

@Entity(tableName = "TutorStudents",
        foreignKeys = {
                @ForeignKey(entity = Student.class,
                        parentColumns = "Id",
                        childColumns = "StudentId"
                ),
                @ForeignKey(entity = Tutor.class,
                        parentColumns = "Id",
                        childColumns = "TutorId"
                ),
                @ForeignKey(entity = Relationship.class,
                        parentColumns = "Id",
                        childColumns = "RelationshipId"
                )
        },
        primaryKeys = {"TutorId", "StudentId"},
        indices = {@Index("StudentId"), @Index("RelationshipId")})
public class TutorStudents {
    @NonNull
    private String TutorId;
    @NonNull
    private String StudentId;
    private int RelationshipId;
    private boolean CurrentTutor;

    public int getRelationshipId() {
        return RelationshipId;
    }

    public void setRelationshipId(int relationshipId) {
        RelationshipId = relationshipId;
    }

    @NonNull
    public String getTutorId() {
        return TutorId;
    }

    public void setTutorId(@NonNull String tutorId) {
        TutorId = tutorId;
    }

    @NonNull
    public String getStudentId() {
        return StudentId;
    }

    public void setStudentId(@NonNull String studentId) {
        StudentId = studentId;
    }

    public boolean isCurrentTutor() {
        return CurrentTutor;
    }

    public void setCurrentTutor(boolean currentTutor) {
        CurrentTutor = currentTutor;
    }
}
