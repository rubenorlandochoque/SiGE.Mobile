package com.smartapps.gestionescolar.models.tutorStudents;

import android.app.Application;
import android.arch.lifecycle.LiveData;

import com.smartapps.gestionescolar.database.RoomDatabase;

import java.util.List;

public class TutorStudentsRepository {
    private TutorStudentsDao tutorStudentsDao;
    private LiveData<List<TutorStudents>> all;

    public TutorStudentsRepository(Application application) {
        RoomDatabase db = RoomDatabase.getDatabase(application);
        tutorStudentsDao = db.tutorStudentsDao();
        all = tutorStudentsDao.getAll();

    }

    public void insert(TutorStudents person) {
        tutorStudentsDao.insert(person);
    }

    public LiveData<List<TutorStudents>> getAll() {
        return tutorStudentsDao.getAll();
    }

    public void disableAll(String studentId) {
        tutorStudentsDao.disableAll(studentId);
    }
}
