package com.smartapps.gestionescolar.models.studentdivision;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.smartapps.gestionescolar.models.base.BaseDao;

import java.util.List;


@Dao
public abstract class StudentDivisionsDao implements BaseDao<StudentDivisions> {

    @Query("select * from StudentDivisions")
    abstract LiveData<List<StudentDivisions>> getAll();

    @Query("delete from StudentDivisions")
    abstract void deleteAll();

    @Query("select * from StudentDivisions where StudentId = :studentId and DivisionId=:divisionId")
    abstract StudentDivisions get(String studentId, int divisionId);

    @Query("update StudentDivisions set currentDivision=0 where StudentId = :studentId")
    public abstract void disableAll(String studentId);
}
