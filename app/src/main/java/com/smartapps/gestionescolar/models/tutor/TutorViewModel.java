package com.smartapps.gestionescolar.models.tutor;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.smartapps.gestionescolar.models.people.PersonRepository;
import com.smartapps.gestionescolar.models.students.Student;
import com.smartapps.gestionescolar.models.tutor.pojo.TutorData;
import com.smartapps.gestionescolar.models.tutor.pojo.TutorDataToSync;

import java.util.List;

import io.reactivex.Maybe;

public class TutorViewModel extends AndroidViewModel {

    private PersonRepository personRepository;
    private TutorRepository tutorRepository;
    // Using LiveData and caching what getAlphabetizedWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    private LiveData<List<Student>> all;

    public TutorViewModel(Application application) {
        super(application);
        tutorRepository = new TutorRepository(application);
        personRepository = new PersonRepository(application);

    }

    public Maybe<TutorData> getTutorData(String tutorId, String studentId) {
        return tutorRepository.getTutorData(tutorId, studentId);
    }

    public TutorDataToSync getTutorDataToSync(String tutorId, String studentId) {
        return tutorRepository.getTutorDataToSync(tutorId, studentId);
    }

    public void insert(Tutor word) {
        new Thread(() -> {
            tutorRepository.insert(word);
        }).start();
    }

    public Maybe<Tutor> getTutor(String id) {
        return tutorRepository.getTutor(id);
    }
}
