package com.smartapps.gestionescolar.models.base;

import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Update;


public interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(T obect);

    @Insert
    void insert(T... object);

    @Update
    void update(T object);

    @Delete
    void delete(T object);
}
