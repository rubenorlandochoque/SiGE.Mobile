package com.smartapps.gestionescolar.models.shifts;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.smartapps.gestionescolar.models.base.BaseDao;

import java.util.List;

@Dao
public abstract class ShiftDao implements BaseDao<Shift> {
    @Query("select * from Shifts")
    public abstract LiveData<List<Shift>> getAll();

    @Query("delete from Shifts")
    public abstract void deleteAll();
}