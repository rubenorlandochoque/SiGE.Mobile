package com.smartapps.gestionescolar.models.studentdivision;

import android.app.Application;
import android.arch.lifecycle.LiveData;

import com.smartapps.gestionescolar.database.RoomDatabase;

import java.util.List;

public class StudentDivisionsRepository {
    private StudentDivisionsDao studentsDivisionDao;
    private LiveData<List<StudentDivisions>> all;

    public StudentDivisionsRepository(Application application) {
        RoomDatabase db = RoomDatabase.getDatabase(application);
        studentsDivisionDao = db.studentDivisionsDao();
        all = studentsDivisionDao.getAll();

    }

    public void insert(StudentDivisions object) {
        studentsDivisionDao.insert(object);
    }

    public StudentDivisions get(String studentId, int divisionId) {
        return studentsDivisionDao.get(studentId, divisionId);
    }

    public void delete(StudentDivisions studentDivisions) {
        studentsDivisionDao.delete(studentDivisions);
    }

    public LiveData<List<StudentDivisions>> getAll() {
        return studentsDivisionDao.getAll();
    }

    public void disableAll(String studentId) {
        studentsDivisionDao.disableAll(studentId);
    }
}
