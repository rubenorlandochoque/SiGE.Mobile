package com.smartapps.gestionescolar.models.base;

import android.app.Application;

import com.smartapps.gestionescolar.database.RoomDatabase;
import com.smartapps.gestionescolar.models.courseyear.CourseYearDao;
import com.smartapps.gestionescolar.models.division.DivisionDao;
import com.smartapps.gestionescolar.models.persongenders.PersonGendersDao;
import com.smartapps.gestionescolar.models.relationship.RelationshipDao;

import io.reactivex.Completable;

public class BaseRepository {
    private final CourseYearDao courseYearDao;
    private final DivisionDao divisionDao;
    private final PersonGendersDao personGendersDao;
    private final RelationshipDao relationshipDao;

    BaseRepository(Application application) {
        RoomDatabase db = RoomDatabase.getDatabase(application);
        courseYearDao = db.courseYearDao();
        divisionDao = db.divisionDao();
        personGendersDao = db.personGendersDao();
        relationshipDao = db.relationshipDao();
    }

    public Completable seed() {
        return Completable.fromAction(() -> {
//            Seed seed = new Seed();
//            seed.seedCourseYear(courseYearDao);
//            seed.seedDivision(divisionDao);
//            seed.seedPesonGenders(personGendersDao);
//            seed.seedRelationship(relationshipDao);
        });
    }
}
