package com.smartapps.gestionescolar.models.courseyear;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "CourseYears")
public class CourseYear {
    @PrimaryKey
    @NonNull
    private int Id;
    private String CourseYearName;

    public String getCourseYearName() {
        return CourseYearName;
    }

    public void setCourseYearName(String courseYearName) {
        CourseYearName = courseYearName;
    }

    public int getId() {
        return this.Id;
    }

    public void setId(@NonNull int Id) {
        this.Id = Id;
    }
}
