package com.smartapps.gestionescolar.models.courseyear.pojo;

public class CourseYearAndAllDivisions {
    public int DivisionId;
    public String DivisionName;
    public int CourseYearId;
    public String CourseYearName;
}
