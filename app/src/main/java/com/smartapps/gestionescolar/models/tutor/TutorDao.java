package com.smartapps.gestionescolar.models.tutor;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.smartapps.gestionescolar.models.base.BaseDao;
import com.smartapps.gestionescolar.models.tutor.pojo.TutorData;
import com.smartapps.gestionescolar.models.tutor.pojo.TutorDataToSync;

import io.reactivex.Maybe;

@Dao
public interface TutorDao extends BaseDao<Tutor> {
    @Query("SELECT * from Tutors where Id = :id")
    Tutor getTutorById(String id);

    @Query("SELECT p.*,ts.RelationshipId, t.Ocupation from Tutors as t " +
            "inner join people as p on p.Id = t.Id " +
            "inner join tutorstudents as ts on ts.TutorId = t.Id and ts.StudentId=:studentId and ts.currenttutor  = 1 " +
            "where t.Id = :tutorId")
    Maybe<TutorData> getTutorData(String tutorId, String studentId);

    @Query("SELECT p.*,ts.RelationshipId, t.Ocupation from Tutors as t " +
            "inner join people as p on p.Id = t.Id " +
            "inner join tutorstudents as ts on ts.TutorId = t.Id and ts.StudentId=:studentId and ts.currenttutor  = 1 " +
            "where t.Id = :tutorId")
    TutorDataToSync getTutorDataToSync(String tutorId, String studentId);

    @Query("SELECT * from Tutors where Id = :id")
    Maybe<Tutor> getTutor(String id);
}
