package com.smartapps.gestionescolar.models.typeemployee;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.smartapps.gestionescolar.models.base.BaseDao;

import java.util.List;

@Dao
public abstract class TypeEmployeeDao implements BaseDao<TypeEmployee> {

    @Query("select * from TypeEmployees")
    public abstract LiveData<List<TypeEmployee>> getAll();

    @Query("delete from TypeEmployees")
    public abstract void deleteAll();
}
