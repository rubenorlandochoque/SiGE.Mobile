package com.smartapps.gestionescolar.models.studentdivision;

import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

@Entity(tableName = "StudentDivisions", primaryKeys =
        {"StudentId", "DivisionId"})
public class StudentDivisions {
    @NonNull
    private String StudentId;
    @NonNull
    private int DivisionId;
    private boolean currentDivision;

    @NonNull
    public String getStudentId() {
        return StudentId;
    }

    public void setStudentId(@NonNull String studentId) {
        StudentId = studentId;
    }

    public int getDivisionId() {
        return DivisionId;
    }

    public void setDivisionId(int divisionId) {
        DivisionId = divisionId;
    }

    public boolean isCurrentDivision() {
        return currentDivision;
    }

    public void setCurrentDivision(boolean currentDivision) {
        this.currentDivision = currentDivision;
    }
}
