package com.smartapps.gestionescolar.models.teacher.pojo;

import java.util.Date;

public class TeacherListData {
    public String FirstName;
    public String LastName;
    public String DocumentNumber;
    public Date Birthdate;
    public String Id;
}
