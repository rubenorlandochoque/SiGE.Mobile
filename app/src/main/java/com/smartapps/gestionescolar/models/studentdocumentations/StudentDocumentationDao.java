package com.smartapps.gestionescolar.models.studentdocumentations;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.smartapps.gestionescolar.models.base.BaseDao;
import com.smartapps.gestionescolar.models.students.pojo.StudentData;

import java.util.List;

import io.reactivex.Maybe;


@Dao
public abstract class StudentDocumentationDao implements BaseDao<StudentDocumentation> {

    @Query("select * from StudentDocumentations")
    abstract LiveData<List<StudentDocumentation>> getAll();

    @Query("delete from StudentDocumentations")
    abstract void deleteAll();

    @Query("select * from StudentDocumentations where StudentId = :studentId")
    abstract StudentDocumentation get(String studentId);

    @Query("SELECT DocumentationId from StudentDocumentations where StudentId=:studentId")
    abstract Maybe<List<Integer>> getDocumentations(String studentId);

    @Query("delete from StudentDocumentations where StudentId=:studentId")
    abstract void deleteAll(String studentId);

    @Query("SELECT DocumentationId from StudentDocumentations where StudentId=:studentId")
    abstract List<Integer> getStudentDocumentationsToSync(String studentId);
}
