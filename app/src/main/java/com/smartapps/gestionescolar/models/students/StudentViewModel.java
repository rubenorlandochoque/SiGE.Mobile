package com.smartapps.gestionescolar.models.students;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.smartapps.gestionescolar.models.documentations.Documentation;
import com.smartapps.gestionescolar.models.people.Person;
import com.smartapps.gestionescolar.models.people.PersonRepository;
import com.smartapps.gestionescolar.models.studentdivision.StudentDivisions;
import com.smartapps.gestionescolar.models.studentdivision.StudentDivisionsRepository;
import com.smartapps.gestionescolar.models.studentdocumentations.StudentDocumentation;
import com.smartapps.gestionescolar.models.studentdocumentations.StudentDocumentationRepository;
import com.smartapps.gestionescolar.models.students.pojo.ImageToSync;
import com.smartapps.gestionescolar.models.students.pojo.StudentData;
import com.smartapps.gestionescolar.models.students.pojo.StudentDataSync;
import com.smartapps.gestionescolar.models.students.pojo.StudentListData;
import com.smartapps.gestionescolar.models.tutor.Tutor;
import com.smartapps.gestionescolar.models.tutor.TutorRepository;
import com.smartapps.gestionescolar.models.tutor.pojo.TutorData;
import com.smartapps.gestionescolar.models.tutor.pojo.TutorDataToSync;
import com.smartapps.gestionescolar.models.tutorStudents.TutorStudents;
import com.smartapps.gestionescolar.models.tutorStudents.TutorStudentsRepository;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;

public class StudentViewModel extends AndroidViewModel {

    private PersonRepository personRepository;
    private StudentRepository studentRepository;
    private StudentDivisionsRepository studentDivisionsRepository;
    private TutorRepository tutorRepository;
    private TutorStudentsRepository tutorStudentsRepository;
    private StudentDocumentationRepository studentDocumentationRepository;
    // Using LiveData and caching what getAlphabetizedWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    private LiveData<List<Student>> all;

    public StudentViewModel(Application application) {
        super(application);
        studentRepository = new StudentRepository(application);
        personRepository = new PersonRepository(application);
        studentDivisionsRepository = new StudentDivisionsRepository(application);
        tutorRepository = new TutorRepository(application);
        tutorStudentsRepository = new TutorStudentsRepository(application);
        studentDocumentationRepository = new StudentDocumentationRepository(application);
    }

    public void insert(Student word) {
        new Thread(() -> {
            studentRepository.insert(word);
        }).start();
    }

    public Maybe<StudentData> getStudent(String id) {
        return studentRepository.getStudent(id);
    }

    public LiveData<List<StudentListData>> getStudentList(int courseYearId, int shiftId) {
        return studentRepository.getStudentList(courseYearId, shiftId);
    }

    public Single<String> saveStudent(StudentData studentData, TutorData tutorData) {
        return Single.create(emitter -> {
            // Save Student
            try {
                Person person = personRepository.getByDni(studentData.getDocumentNumber());
                if (person == null) {
                    person = new Person();
                    String id = UUID.randomUUID().toString();
                    person.setId(id);
                }
                person.setFirstName(studentData.getFirstName());
                person.setLastName(studentData.getLastName());
                person.setDocumentNumber(studentData.getDocumentNumber());
                person.setPersonGenderId(studentData.getPersonGenderId());
                person.setPhoneNumber(studentData.getPhoneNumber());
                person.setMobilePhoneNumber(studentData.getMobilePhoneNumber());
                person.setAnotherContactPhone(studentData.getAnotherContactPhone());
                person.setAddress(studentData.getAddress());
                person.setBirthdate(studentData.getBirthdate());
                person.setNationality(studentData.getNationality());
                person.setLocation(studentData.getLocation());

                person.setBelongsEthnicGroup(studentData.isBelongsEthnicGroup());
                person.setEthnicName(studentData.getEthnicName());
                person.setHasHealthProblems(studentData.isHasHealthProblems());
                person.setDescriptionHealthProblems(studentData.getDescriptionHealthProblems());
                person.setHasLegalProblems(studentData.isHasLegalProblems());
                person.setDescriptionLegalProblems(studentData.getDescriptionLegalProblems());

                if (!studentData.getScannerResult().trim().isEmpty()) {
                    person.setScannerResult(studentData.getScannerResult());
                }
                personRepository.insert(person);

                Student student = studentRepository.getStudentById(person.getId());
                if (student == null) {
                    student = new Student();
                    student.setId(person.getId());
                }
                student.setSync(false);
                if(student.getIdCardBack() == null || !student.getIdCardBack().equals(studentData.IdCardBack)){
                    student.setSyncCardBack(false);
                }
                if(student.getIdCardFront() == null || !student.getIdCardFront().equals(studentData.IdCardFront)){
                    student.setSyncCardFront(false);
                }

                student.setHasBrothersOfSchoolAge(studentData.HasBrothersOfSchoolAge);
                student.setIdCardBack(studentData.IdCardBack);
                student.setIdCardFront(studentData.IdCardFront);
                student.setSchoolOrigin(studentData.SchoolOrigin);
                student.setObservation(studentData.Observation);
                student.setOtherPhoneBelongs(studentData.OtherPhoneBelongs);

                studentRepository.insert(student);

                // studen division
                StudentDivisions studentDivisions = new StudentDivisions();
                studentDivisions.setStudentId(student.getId());
                studentDivisions.setDivisionId(studentData.DivisionId);
                studentDivisions.setCurrentDivision(true);
                studentDivisionsRepository.disableAll(student.getId()); // deshabilitar anteriores
                studentDivisionsRepository.insert(studentDivisions); // insertar nuevo.

                // student documentations
                studentDocumentationRepository.deleteAll(student.getId());
                for (int documentation : studentData.documentations) {
                    StudentDocumentation studentDocumentation = new StudentDocumentation();
                    studentDocumentation.setDocumentationId(documentation);
                    studentDocumentation.setStudentId(student.getId());
                    studentDocumentationRepository.insert(studentDocumentation);
                }

                //save Tutor
                person = personRepository.getByDni(tutorData.getDocumentNumber());
                if (person == null) {
                    person = new Person();
                    String id = UUID.randomUUID().toString();
                    person.setId(id);
                }
                person.setFirstName(tutorData.getFirstName());
                person.setLastName(tutorData.getLastName());
                person.setDocumentNumber(tutorData.getDocumentNumber());
                person.setPersonGenderId(tutorData.getPersonGenderId());
                person.setPhoneNumber(tutorData.getPhoneNumber());
                person.setMobilePhoneNumber(tutorData.getMobilePhoneNumber());
                person.setAnotherContactPhone(tutorData.getAnotherContactPhone());
                person.setAddress(tutorData.getAddress());
                person.setBirthdate(tutorData.getBirthdate());
                person.setNationality(tutorData.getNationality());
                person.setLocation(tutorData.getLocation());
                if (!tutorData.getScannerResult().trim().isEmpty()) {
                    person.setScannerResult(tutorData.getScannerResult());
                }
                personRepository.insert(person);

                Tutor tutor = tutorRepository.getTutorById(person.getId());
                if (tutor == null) {
                    tutor = new Tutor();
                    tutor.setId(person.getId());
                }
                tutor.setSync(false);
                tutor.setOcupation(tutorData.getOcupation());
                tutorRepository.insert(tutor);

                // save tutor-student
                TutorStudents tutorStudents = new TutorStudents();
                tutorStudents.setStudentId(student.getId());
                tutorStudents.setTutorId(tutor.getId());
                tutorStudents.setCurrentTutor(true);
                tutorStudents.setRelationshipId(tutorData.getRelationshipId());
                tutorStudentsRepository.disableAll(student.getId()); // deshabilitar anteriores
                tutorStudentsRepository.insert(tutorStudents); // insertar nuevo.

                emitter.onSuccess(tutor.getId());
            } catch (Exception e) {
                emitter.onError(e);
            }
        });
    }

//    public Completable saveStudent(StudentData studentData, TutorData tutorData) {
//
//        return Completable.fromAction(() -> {
//            // if there's no use, create a new user.
//            // if we already have a user, then, since the user object is immutable,
//            // create a new user, with the id of the previous user and the updated user name.
//        });
//    }

    public Single<List<StudentDataSync>> getDataToSync() {

        return Single.create(new SingleOnSubscribe<List<StudentDataSync>>() {
            @Override
            public void subscribe(SingleEmitter<List<StudentDataSync>> emitter) throws Exception {
                List<StudentDataSync> data = studentRepository.getStudentToSync();
                for (StudentDataSync studentDataSync : data) {
                    List<Integer> documentations = studentDocumentationRepository.getStudentDocumentationsToSync(studentDataSync.getId());
                    studentDataSync.setDocumentations(documentations);

                    if (studentDataSync.getIdCardBack() != null && !studentDataSync.getIdCardBack().isEmpty()) {
                        try {
                            String[] s = studentDataSync.getIdCardBack().split(File.separator);
                            studentDataSync.setIdCardBack(s[s.length - 1]);
                        } catch (Exception ignored) {
                        }
                    }
                    if (studentDataSync.getIdCardFront() != null && !studentDataSync.getIdCardFront().isEmpty()) {
                        try {
                            String[] s = studentDataSync.getIdCardFront().split(File.separator);
                            studentDataSync.setIdCardFront(s[s.length - 1]);
                        } catch (Exception ignored) {
                        }
                    }
                    TutorDataToSync tutorDataToSync = tutorRepository.getTutorDataToSync(studentDataSync.getTutorId(), studentDataSync.getId());
                    studentDataSync.setTutorDataToSync(tutorDataToSync);
                }

                emitter.onSuccess(data);
            }
        });
    }

    public Single<List<ImageToSync>> getImageToSync() {

        return Single.create(new SingleOnSubscribe<List<ImageToSync>>() {
            @Override
            public void subscribe(SingleEmitter<List<ImageToSync>> emitter) throws Exception {
                List<ImageToSync> front = studentRepository.getCardFrontImageToSync();
                List<ImageToSync> back = studentRepository.getCardBackImageToSync();
                List<ImageToSync> list = new ArrayList<>();
                list.addAll(front);
                list.addAll(back);
                emitter.onSuccess(list);
            }
        });
    }


    public Single<String> syncStudentTutor(String studentId, String tutorId) {
        return Single.create(emitter -> {
            try {
                Student student = studentRepository.getStudentById(studentId);
                if (student != null) {
                    student.setSync(true);
                    studentRepository.insert(student);
                }

                Tutor tutor = tutorRepository.getTutorById(tutorId);
                if (tutor != null) {
                    tutor.setSync(true);
                    tutorRepository.insert(tutor);
                }
                emitter.onSuccess("success");
            } catch (Exception e) {
                emitter.onError(e);
            }
        });
    }

    public Single<String> syncImage(ImageToSync item) {
        return Single.create(emitter -> {
            try {
                Student student = studentRepository.getStudentById(item.getId());
                if (student != null) {
                    switch (item.getType()) {
                        case ImageToSync.CARD_BACK:
                            student.setSyncCardBack(true);
                            break;
                        case ImageToSync.CARD_FRONT:
                            student.setSyncCardFront(true);
                            break;
                    }

                    studentRepository.insert(student);
                }
                emitter.onSuccess("success");
            } catch (Exception e) {
                emitter.onError(e);
            }
        });
    }
}
