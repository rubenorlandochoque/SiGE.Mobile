package com.smartapps.gestionescolar.models.studentdocumentations;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;


import java.util.List;

import io.reactivex.Maybe;

public class StudentDocumentationViewModel extends AndroidViewModel {
    private StudentDocumentationRepository mRepository;
    // Using LiveData and caching what getAlphabetizedWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.

    public StudentDocumentationViewModel(Application application) {
        super(application);
        mRepository = new StudentDocumentationRepository(application);
    }

    public void insert(StudentDocumentation studentDocumentation) {
        new Thread(() -> {
            mRepository.insert(studentDocumentation);
        }).start();
    }


    public LiveData<List<StudentDocumentation>> getAll() {
        return mRepository.getAll();
    }

    public Maybe<List<Integer>> getDocumentations(String id) {
        return mRepository.getDocumentations(id);
    }
}
