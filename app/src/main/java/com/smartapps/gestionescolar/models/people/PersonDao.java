package com.smartapps.gestionescolar.models.people;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.smartapps.gestionescolar.models.base.BaseDao;

import java.util.List;

@Dao
public abstract class PersonDao implements BaseDao<Person> {
    @Query("select * from People where DocumentNumber=:documentNumber")
    abstract Person getByDni(String documentNumber);

    @Query("select * from People where Id=:id")
    abstract Person getById(String id);

    @Insert
    abstract void insertAll(List<Person> persons);

    @Query("select * from People")
    abstract LiveData<List<Person>> getAll();
}