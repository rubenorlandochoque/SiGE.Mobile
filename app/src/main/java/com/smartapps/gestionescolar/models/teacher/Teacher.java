package com.smartapps.gestionescolar.models.teacher;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.smartapps.gestionescolar.models.people.Person;

import java.util.Date;

@Entity(tableName = "Staff", foreignKeys = {
        @ForeignKey(entity = Person.class,
                parentColumns = "Id",
                childColumns = "Id"
        )
})
public class Teacher {
    @PrimaryKey
    @NonNull
    private String Id;
    private String CUIL;
    private Date HireDate;
    private boolean Sync = false;

    public String getCUIL() {
        return CUIL;
    }

    public void setCUIL(String CUIL) {
        this.CUIL = CUIL;
    }

    public Date getHireDate() {
        return HireDate;
    }

    public void setHireDate(Date hireDate) {
        HireDate = hireDate;
    }

    public boolean isSync() {
        return Sync;
    }

    public void setSync(boolean sync) {
        Sync = sync;
    }

    @NonNull
    public String getId() {
        return Id;
    }

    public void setId(@NonNull String id) {
        Id = id;
    }
}
