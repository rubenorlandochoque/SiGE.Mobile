package com.smartapps.gestionescolar.models.students.pojo;

import android.arch.persistence.room.Ignore;

import com.smartapps.gestionescolar.models.people.Person;
import com.smartapps.gestionescolar.models.tutor.pojo.TutorDataToSync;

import java.util.List;

public class StudentDataSync extends Person {
    private boolean HasBrothersOfSchoolAge;
    private int DivisionId;
    private String TutorId;
    private String IdCardFront;
    private String IdCardBack;
    private String Observation;
    private String SchoolOrigin;
    private String OtherPhoneBelongs;
    @Ignore
    private TutorDataToSync TutorDataToSync;
    @Ignore
    private List<Integer> Documentations;

    public String getOtherPhoneBelongs() {
        return OtherPhoneBelongs;
    }

    public void setOtherPhoneBelongs(String otherPhoneBelongs) {
        OtherPhoneBelongs = otherPhoneBelongs;
    }

    public List<Integer> getDocumentations() {
        return Documentations;
    }

    public void setDocumentations(List<Integer> documentations) {
        Documentations = documentations;
    }

    public String getObservation() {
        return Observation;
    }

    public void setObservation(String observation) {
        Observation = observation;
    }

    public String getSchoolOrigin() {
        return SchoolOrigin;
    }

    public void setSchoolOrigin(String schoolOrigin) {
        SchoolOrigin = schoolOrigin;
    }

    public String getIdCardFront() {
        return IdCardFront;
    }

    public void setIdCardFront(String idCardFront) {
        IdCardFront = idCardFront;
    }

    public String getIdCardBack() {
        return IdCardBack;
    }

    public void setIdCardBack(String idCardBack) {
        IdCardBack = idCardBack;
    }

    public String getTutorId() {
        return TutorId;
    }

    public void setTutorId(String tutorId) {
        TutorId = tutorId;
    }

    public TutorDataToSync getTutorDataToSync() {
        return TutorDataToSync;
    }

    public void setTutorDataToSync(TutorDataToSync TutorDataToSync) {
        this.TutorDataToSync = TutorDataToSync;
    }

    public boolean isHasBrothersOfSchoolAge() {
        return HasBrothersOfSchoolAge;
    }

    public void setHasBrothersOfSchoolAge(boolean hasBrothersOfSchoolAge) {
        HasBrothersOfSchoolAge = hasBrothersOfSchoolAge;
    }

    public int getDivisionId() {
        return DivisionId;
    }

    public void setDivisionId(int divisionId) {
        DivisionId = divisionId;
    }
}
