package com.smartapps.gestionescolar.models.courseyear;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import com.smartapps.gestionescolar.models.base.BaseDao;
import com.smartapps.gestionescolar.models.courseyear.pojo.CourseYearAndAllDivisions;
import com.smartapps.gestionescolar.models.courseyear.pojo.CourseYearStudentsCount;

import java.util.List;

import io.reactivex.Single;


@Dao
public abstract class CourseYearDao implements BaseDao<CourseYear> {
    @Query("select * from CourseYears")
    public abstract LiveData<List<CourseYear>> getAll();

    @Query("delete from CourseYears")
    public abstract void deleteAll();

    @Query("select count(sd.StudentId) as Cant,cy.Id as CourseYearId, CourseYearName " +
            "from CourseYears as cy " +
            "left join Divisions as d on d.CourseYearId = cy.ID " +
            "left join StudentDivisions as sd on sd.DivisionId = d.Id and sd.currentDivision = 1 where d.ShiftId = :shiftId " +
            "group by cy.Id,cy.CourseYearName")
    public abstract LiveData<List<CourseYearStudentsCount>> getAllWithStudentsCount(int shiftId);


    @Transaction
    @Query("SELECT d.Id as DivisionId, d.DivisionName, d.CourseYearId, cy.CourseYearName from Divisions as d " +
            "inner join CourseYears as cy on cy.Id = d.CourseYearId and d.ShiftId = :shiftId ")
    public abstract LiveData<List<CourseYearAndAllDivisions>> getCourseYearsDivisionsByShiftId(int shiftId);

    @Query("select * from CourseYears")
    public abstract Single<List<CourseYear>> getAllSingle();
}
