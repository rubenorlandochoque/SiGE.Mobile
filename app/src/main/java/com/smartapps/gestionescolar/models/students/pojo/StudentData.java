package com.smartapps.gestionescolar.models.students.pojo;

import android.arch.persistence.room.Ignore;

import com.smartapps.gestionescolar.models.people.Person;

import java.util.ArrayList;
import java.util.List;

public class StudentData extends Person {
    public boolean HasBrothersOfSchoolAge;
    public int DivisionId;
    public String IdCardFront;
    public String IdCardBack;
    public String Observation;
    public String SchoolOrigin;
    public String OtherPhoneBelongs;

    @Ignore
    public List<Integer> documentations = new ArrayList<>();

    // campo solo para verificar que se repondio la pregunta has brother, ya que es obligatoria
    @Ignore
    public boolean HasBrothersOfSchoolAgeAnswered;
    @Ignore
    public boolean BelongsEthnicGroupAnswered;
    @Ignore
    public boolean HasHealthProblemsAnswered;
    @Ignore
    public boolean HasLegalProblemsAnswered;
}
