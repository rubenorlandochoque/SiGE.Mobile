package com.smartapps.gestionescolar.models.students;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.smartapps.gestionescolar.models.base.BaseDao;
import com.smartapps.gestionescolar.models.students.pojo.ImageToSync;
import com.smartapps.gestionescolar.models.students.pojo.StudentData;
import com.smartapps.gestionescolar.models.students.pojo.StudentDataSync;
import com.smartapps.gestionescolar.models.students.pojo.StudentListData;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Single;

@Dao
public interface StudentDao extends BaseDao<Student> {
    @Query("SELECT * from Students " +
            "inner join People on Students.Id = People.Id " +
            "inner join StudentDivisions on Students.Id = StudentDivisions.StudentId " +
            "where Students.Id = :id " +
            "and StudentDivisions.currentDivision = 1 ")
    Maybe<StudentData> getStudent(String id);

    @Query("SELECT p.FirstName, p.LastName,p.DocumentNumber,p.Birthdate,p.Id, ts.TutorId as TutorId from StudentDivisions as sd " +
            "inner join divisions as d on sd.DivisionId = d.Id and sd.currentDivision = 1 " +
            "inner join students as s on s.Id = sd.StudentId " +
            "inner join people as p on p.Id = s.Id " +
            "inner join tutorstudents as ts on ts.StudentId = s.Id and ts.CurrentTutor = 1 " +
            "where d.CourseYearId = :courseYearId and d.ShiftId = :shiftId")
    LiveData<List<StudentListData>> getStudentList(int courseYearId, int shiftId);

    @Query("SELECT * from Students where Id = :id")
    Student getStudentById(String id);

    @Query("select p.*,s.HasBrothersOfSchoolAge,sd.DivisionId, ts.tutorid as TutorId, s.IdCardBack, s.IdCardFront,s.Observation, s.SchoolOrigin, s.OtherPhoneBelongs from students as s " +
            "inner join studentDivisions as sd on sd.studentid = s.id and currentdivision = 1 " +
            "inner join tutorStudents as ts on ts.studentid = s.id and ts.currenttutor  = 1 " +
            "inner join people as p on p.id = s.id where s.Sync = 0")
    List<StudentDataSync> getStudentToSync();


    @Query("select IdCardFront as Path, Id, 1 as Type from Students where SyncCardFront = 0 and trim(ifnull(IdCardFront, '')) != ''")
    List<ImageToSync> getCardFrontImageToSync();

    @Query("select IdCardBack as Path, Id, 2 as Type from Students where SyncCardBack = 0 and trim(ifnull(IdCardBack, '')) != ''")
    List<ImageToSync> getCardBackImageToSync();
}
