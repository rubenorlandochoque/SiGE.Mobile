package com.smartapps.gestionescolar.services;

import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.smartapps.gestionescolar.classes.SharedConfig;
import com.smartapps.gestionescolar.models.students.StudentViewModel;
import com.smartapps.gestionescolar.models.students.pojo.ImageToSync;
import com.smartapps.gestionescolar.util.Util;

import org.json.JSONObject;

import java.io.File;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.TimeUnit;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.Response;

public class UploadImageService {
    private final String TAG = "DownloadService";
    private Stack<ImageToSync> studentDataSyncs = new Stack<>();
    private UploadStudentListener uploadStudentListener;
    private StudentViewModel studentViewModel;
    private boolean success = true;

    public UploadImageService(UploadStudentListener uploadStudentListener) {
        this.uploadStudentListener = uploadStudentListener;
    }

    private void uploadUpdate(ImageToSync item) {
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(24, TimeUnit.HOURS)
                .readTimeout(24, TimeUnit.HOURS)
                .writeTimeout(24, TimeUnit.HOURS)
                .build();

        AndroidNetworking.upload(SharedConfig.getServerUrl()
                + "/api/Upload/PostStudentImage")
                .addMultipartFile("file", new File(item.getPath()))
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .setOkHttpClient(okHttpClient)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                    }
                })
                .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                    @Override
                    public void onResponse(Response okHttpResponse, JSONObject response) {
                        Log.e("e", response.toString());
                        if (okHttpResponse.code() == 201) {
                            UploadImageService.this.studentViewModel.syncImage(item)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new SingleObserver<String>() {
                                        @Override
                                        public void onSubscribe(Disposable d) {

                                        }

                                        @Override
                                        public void onSuccess(String s) {
                                            Util.borrarArchivo(item.getPath());
                                            next();
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            success = false;
                                            next();
                                        }
                                    });
                        } else {
                            success = false;
                            next();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("e", anError.toString());
                        success = false;
                        next();
                    }
                });
    }

    public void uploadImageStudents(StudentViewModel studentViewModel) {
        this.studentViewModel = studentViewModel;
        this.studentViewModel.getImageToSync().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<List<ImageToSync>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(List<ImageToSync> studentDataSyncs) {
                Stack<ImageToSync> stack = new Stack<>();
                stack.addAll(studentDataSyncs);
                UploadImageService.this.studentDataSyncs = stack;
                next();
            }

            @Override
            public void onError(Throwable e) {
                uploadStudentListener.onFinish(false);
            }
        });
    }

    private void next() {
        if (studentDataSyncs.size() > 0) {
            uploadUpdate(UploadImageService.this.studentDataSyncs.pop());
        } else {
            this.uploadStudentListener.onFinish(success);
        }
    }

    public interface UploadStudentListener<T> {
        void onFinish(boolean success);
    }
}
