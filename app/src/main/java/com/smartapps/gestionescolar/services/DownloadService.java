package com.smartapps.gestionescolar.services;

import com.rx2androidnetworking.Rx2AndroidNetworking;
import com.smartapps.gestionescolar.classes.SharedConfig;

import org.json.JSONObject;

import io.reactivex.Observable;

public class DownloadService {
    public DownloadService() {
    }

    public Observable<JSONObject> rxDownload(String api) {
        final String urlServer = SharedConfig.getServerUrl() + api;
        return Rx2AndroidNetworking.get(urlServer)
                .addHeaders("Authorization", "Bearer " + SharedConfig.getToken())
                .build()
                .getJSONObjectObservable();
    }
}
