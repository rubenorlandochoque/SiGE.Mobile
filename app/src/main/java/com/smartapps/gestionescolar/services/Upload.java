package com.smartapps.gestionescolar.services;

import com.smartapps.gestionescolar.models.students.StudentViewModel;
import com.smartapps.gestionescolar.models.teacher.TeacherViewModel;

public class Upload {
    private StudentViewModel studentViewModel;
    private TeacherViewModel teacherViewModel;
    private UploadListner uploadListner;
    private boolean successImages = true;
    private boolean successStudent = true;
    private boolean successTeacher = true;

    public void Sync(StudentViewModel studentViewModel, TeacherViewModel teacherViewModel, UploadListner uploadListner) {
        this.studentViewModel = studentViewModel;
        this.teacherViewModel = teacherViewModel;
        this.uploadListner = uploadListner;
        syncImages();
    }

    private void syncImages() {
        new UploadImageService((success) -> {
            successImages = success;
            syncStudents();
        }).uploadImageStudents(studentViewModel);
    }

    private void syncStudents() {
        new UploadStudentService((success) -> {
            successStudent = success;
            syncTeachers();
        }).uploadUpdateStudents(studentViewModel);
    }

    private void syncTeachers() {
        new UploadTeacherService((success) -> {
            successTeacher = success;
            uploadListner.finish(successImages, successStudent, successTeacher);
        }).uploadUpdateTeachers(teacherViewModel);
    }

    public interface UploadListner {
        void finish(boolean successImages, boolean successStudent, boolean successTeacher);
    }
}
