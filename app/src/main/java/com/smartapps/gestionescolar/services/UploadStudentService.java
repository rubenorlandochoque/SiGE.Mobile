package com.smartapps.gestionescolar.services;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.smartapps.gestionescolar.classes.SharedConfig;
import com.smartapps.gestionescolar.models.students.StudentViewModel;
import com.smartapps.gestionescolar.models.students.pojo.StudentDataSync;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Stack;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class UploadStudentService {
    private final String TAG = "DownloadService";
    private Stack<StudentDataSync> studentDataSyncs = new Stack<>();
    private UploadStudentListener uploadStudentListener;
    StudentViewModel studentViewModel;
    private boolean success = true;

    public UploadStudentService(UploadStudentListener uploadStudentListener) {
        this.uploadStudentListener = uploadStudentListener;
    }

    private void uploadUpdate(JSONObject item) {
        final String urlServer = SharedConfig.getServerUrl() + "api/students/add";
        AndroidNetworking.post(urlServer)
                .addHeaders("Authorization", "Bearer " + SharedConfig.getToken())
                .addJSONObjectBody(item)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getInt("ResultCode") == 1) {
                                // actualizar sincronizados
                                String studentId = item.getString("Id");
                                JSONObject tutor = item.getJSONObject("Tutor");
                                String tutorId = tutor.getString("Id");
                                UploadStudentService.this.studentViewModel.syncStudentTutor(studentId, tutorId)
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new SingleObserver<String>() {
                                            @Override
                                            public void onSubscribe(Disposable d) {

                                            }

                                            @Override
                                            public void onSuccess(String s) {
                                                next();
                                            }

                                            @Override
                                            public void onError(Throwable e) {
                                                success = false;
                                                next();
                                            }
                                        });
                            } else if (response.getInt("ResultCode") == 0) {
                                success = false;
                                next();
                            }
                        } catch (Exception e) {
                            success = false;
                            next();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        success = false;
                        next();
                    }
                });
    }

    private void sync(StudentDataSync studentDataSync) {

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            JSONObject student = new JSONObject();

            student.put("Id", studentDataSync.getId());
            student.put("FirstName", studentDataSync.getFirstName());
            student.put("LastName", studentDataSync.getLastName());
            student.put("DocumentNumber", studentDataSync.getDocumentNumber());
            student.put("MobilePhoneNumber", studentDataSync.getMobilePhoneNumber());
            student.put("PhoneNumber", studentDataSync.getPhoneNumber());
            student.put("AnotherContactPhone", studentDataSync.getAnotherContactPhone());
            student.put("Birthdate", simpleDateFormat.format(studentDataSync.getBirthdate()));
            student.put("Photo", studentDataSync.getPhoto());
            student.put("PersonGenderId", studentDataSync.getPersonGenderId());
            student.put("DivisionId", studentDataSync.getDivisionId());
            student.put("IdCardBack", studentDataSync.getIdCardBack());
            student.put("IdCardFront", studentDataSync.getIdCardFront());
            student.put("Address", studentDataSync.getAddress());
            student.put("HasBrothersOfSchoolAge", studentDataSync.isHasBrothersOfSchoolAge());
            student.put("ScannerResult", studentDataSync.getScannerResult());
            student.put("Nationality", studentDataSync.getNationality());
            student.put("SchoolOrigin", studentDataSync.getSchoolOrigin());
            student.put("Observation", studentDataSync.getObservation());
            student.put("BelongsEthnicGroup", studentDataSync.isBelongsEthnicGroup());
            student.put("EthnicName", studentDataSync.getEthnicName());
            student.put("HasHealthProblems", studentDataSync.isHasHealthProblems());
            student.put("DescriptionHealthProblems", studentDataSync.getDescriptionHealthProblems());
            student.put("HasLegalProblems", studentDataSync.isHasLegalProblems());
            student.put("DescriptionLegalProblems", studentDataSync.getDescriptionLegalProblems());
            student.put("Location", studentDataSync.getLocation());
            student.put("OtherPhoneBelongs", studentDataSync.getOtherPhoneBelongs());

            JSONArray jsonArrayDocumentatios = new JSONArray();
            for(int doc : studentDataSync.getDocumentations()){
                jsonArrayDocumentatios.put(doc);
            }
            student.put("Documentations", jsonArrayDocumentatios);

            JSONObject tutor = new JSONObject();
            tutor.put("Id", studentDataSync.getTutorDataToSync().getId());
            tutor.put("FirstName", studentDataSync.getTutorDataToSync().getFirstName());
            tutor.put("LastName", studentDataSync.getTutorDataToSync().getLastName());
            tutor.put("DocumentNumber", studentDataSync.getTutorDataToSync().getDocumentNumber());
            tutor.put("MobilePhoneNumber", studentDataSync.getTutorDataToSync().getMobilePhoneNumber());
            tutor.put("PhoneNumber", studentDataSync.getTutorDataToSync().getPhoneNumber());
            tutor.put("AnotherContactPhone", studentDataSync.getTutorDataToSync().getAnotherContactPhone());
            tutor.put("Birthdate", simpleDateFormat.format(studentDataSync.getTutorDataToSync().getBirthdate()));
            tutor.put("Photo", studentDataSync.getTutorDataToSync().getPhoto());
            tutor.put("PersonGenderId", studentDataSync.getTutorDataToSync().getPersonGenderId());
            tutor.put("Address", studentDataSync.getTutorDataToSync().getAddress());
            tutor.put("RelationshipId", studentDataSync.getTutorDataToSync().getRelationshipId());
            tutor.put("ScannerResult", studentDataSync.getTutorDataToSync().getScannerResult());
            tutor.put("Nationality", studentDataSync.getTutorDataToSync().getNationality());
            tutor.put("Ocupation", studentDataSync.getTutorDataToSync().getOcupation());
            tutor.put("Location", studentDataSync.getTutorDataToSync().getLocation());

            student.put("Tutor", tutor);

            uploadUpdate(student);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void uploadUpdateStudents(StudentViewModel studentViewModel) {
        this.studentViewModel = studentViewModel;
        this.studentViewModel.getDataToSync().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<List<StudentDataSync>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(List<StudentDataSync> studentDataSyncs) {
                Stack<StudentDataSync> stack = new Stack<>();
                stack.addAll(studentDataSyncs);
                UploadStudentService.this.studentDataSyncs = stack;
                next();
            }

            @Override
            public void onError(Throwable e) {
                uploadStudentListener.onFinish(false);
            }
        });
    }

    private void next() {
        if (studentDataSyncs.size() > 0) {
            sync(studentDataSyncs.pop());
        } else {
            this.uploadStudentListener.onFinish(success);
        }
    }

    public interface UploadStudentListener<T> {
        void onFinish(boolean result);
    }
}
