package com.smartapps.gestionescolar.ui.studentlist;

import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.smartapps.gestionescolar.R;
import com.smartapps.gestionescolar.classes.SharedConfig;
import com.smartapps.gestionescolar.models.students.StudentViewModel;
import com.smartapps.gestionescolar.models.students.pojo.StudentListData;
import com.smartapps.gestionescolar.models.teacher.TeacherViewModel;
import com.smartapps.gestionescolar.services.Upload;
import com.smartapps.gestionescolar.ui.newstudent.StudentTutorActivity;

import java.util.List;

public class StudentsListByCourseActivity extends AppCompatActivity implements View.OnClickListener {
    private StudentViewModel studentViewModel;
    private int courseYearId = 0;

    private ProgressDialog dialog;
    private TeacherViewModel teacherViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students_list_by_course);

        courseYearId = getIntent().getIntExtra("courseYearId", -1);
        String courseYearName = getIntent().getStringExtra("courseYearName");
        RecyclerView recyclerView = findViewById(R.id.recyclerview_students_list);
        ((TextView) findViewById(R.id.txt_ab_title)).setText(String.format("Alumnos del %s año", courseYearName));
        ((TextView) findViewById(R.id.newStudentByCourseYear)).setText(String.format("Nuevo Alumno del %s año", courseYearName));
        ((TextView) findViewById(R.id.empty_view)).setText(String.format("No hay alumnos en %s año", courseYearName));
        final StudentsListAdapter adapter = new StudentsListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        teacherViewModel = ViewModelProviders.of(this).get(TeacherViewModel.class);

        studentViewModel = ViewModelProviders.of(this).get(StudentViewModel.class);
        studentViewModel.getStudentList(courseYearId, SharedConfig.getShiftId()).observe(this, new Observer<List<StudentListData>>() {
            @Override
            public void onChanged(@Nullable List<StudentListData> studentData) {
                adapter.setCourseYears(studentData);
                adapter.notifyDataSetChanged();
                if (studentData.size() > 0) {
                    findViewById(R.id.recyclerview_students_list).setVisibility(View.VISIBLE);
                    findViewById(R.id.empty_view).setVisibility(View.GONE);
                } else {
                    findViewById(R.id.recyclerview_students_list).setVisibility(View.GONE);
                    findViewById(R.id.empty_view).setVisibility(View.VISIBLE);
                }
            }
        });
        findViewById(R.id.newStudentByCourseYear).setOnClickListener(this);

        findViewById(R.id.sync_button).setVisibility(View.VISIBLE);
        findViewById(R.id.sync_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((dialog == null || !dialog.isShowing())) {
                    dialog = ProgressDialog.show(StudentsListByCourseActivity.this, "",
                            "" +
                                    "Subiendo datos", true);
                }
                new Upload().Sync(studentViewModel, teacherViewModel, new Upload.UploadListner() {
                    @Override
                    public void finish(boolean successImages, boolean successStudent, boolean successTeacher) {
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                        }

                        if (successImages && successStudent && successTeacher) {
                            new MaterialDialog.Builder(StudentsListByCourseActivity.this)
                                    .title("Sincronización completa")
                                    .content("Los datos se sincronizaron correctamente")
                                    .positiveText("Aceptar")
                                    .onPositive((d, which) -> {
                                        d.dismiss();
                                    })
                                    .show();
                        } else {
                            new MaterialDialog.Builder(StudentsListByCourseActivity.this)
                                    .title("Error al sincronizar")
                                    .content("Hubo un error al sincronizar los datos.\nPor favor intente nuevamente.")
                                    .positiveText("Aceptar")
                                    .onPositive((d, which) -> {
                                        d.dismiss();
                                    })
                                    .show();
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.newStudentByCourseYear:
                Intent intent = new Intent(StudentsListByCourseActivity.this, StudentTutorActivity.class);
                intent.putExtra("courseYearId", courseYearId);
                startActivity(intent);
                break;
        }
    }
}
