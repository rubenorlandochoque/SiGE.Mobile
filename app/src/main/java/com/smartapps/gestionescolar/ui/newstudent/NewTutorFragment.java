package com.smartapps.gestionescolar.ui.newstudent;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.smartapps.gestionescolar.R;
import com.smartapps.gestionescolar.models.people.Person;
import com.smartapps.gestionescolar.models.people.PersonViewModel;
import com.smartapps.gestionescolar.models.persongenders.PersonGenders;
import com.smartapps.gestionescolar.models.persongenders.PersonGendersViewModel;
import com.smartapps.gestionescolar.models.relationship.RelationshipViewModel;
import com.smartapps.gestionescolar.models.relationship.pojo.RelationshipData;
import com.smartapps.gestionescolar.models.students.pojo.StudentData;
import com.smartapps.gestionescolar.models.tutor.Tutor;
import com.smartapps.gestionescolar.models.tutor.TutorViewModel;
import com.smartapps.gestionescolar.models.tutor.pojo.TutorData;
import com.smartapps.gestionescolar.ui.main.MainActivity;
import com.smartapps.gestionescolar.ui.scanner.SimpleScannerActivity;
import com.smartapps.gestionescolar.util.Evento;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import io.reactivex.MaybeObserver;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lib.kingja.switchbutton.SwitchMultiButton;

import static br.com.zbra.androidlinq.Linq.stream;

public class NewTutorFragment extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
    public static final int SCANNER_ACTIVITY_RESULT = 3;
    private PersonGendersViewModel personGendersViewModel;
    private TutorViewModel tutorViewModel;
    private RelationshipViewModel relationshipViewModel;
    private PersonViewModel personViewModel;
    List<RelationshipData> relationships = new ArrayList<>();
    List<PersonGenders> personGendersList;
    String[] personGendersString;

    TextView dateTextView;
    Button dateButton;
    ImageButton copyStudentAddressButton;
    ImageButton copyStudentPhoneNumber;
    ImageButton copyStudentMobilePhoneNumber;
    ImageButton copyStudentAnotherPhoneNumber;
    SimpleDateFormat simpleDateFormat;
    View rootView;

    String tutorId = "";
    String studentId = "";
    TextWatcher textWatcher;
    String scannerResult = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        personGendersViewModel = ViewModelProviders.of(this).get(PersonGendersViewModel.class);
        tutorViewModel = ViewModelProviders.of(this).get(TutorViewModel.class);
        relationshipViewModel = ViewModelProviders.of(this).get(RelationshipViewModel.class);
        personViewModel = ViewModelProviders.of(this).get(PersonViewModel.class);


        rootView = inflater.inflate(R.layout.fragment_tutor, container, false);
        ((EditText) rootView.findViewById(R.id.input_firstame)).setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        ((EditText) rootView.findViewById(R.id.input_lastname)).setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        dateButton = rootView.findViewById(R.id.set_date_button);
        copyStudentAddressButton = rootView.findViewById(R.id.copy_student_address);
        copyStudentPhoneNumber = rootView.findViewById(R.id.copy_student_phone_number);
        copyStudentMobilePhoneNumber = rootView.findViewById(R.id.copy_student_mobile_phone_number);
        copyStudentAnotherPhoneNumber = rootView.findViewById(R.id.copy_student_another_phone_number);
        dateTextView = rootView.findViewById(R.id.input_birthdate);
        simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        dateButton.setOnClickListener(view -> showDate(1980, 0, 1, R.style.DatePickerSpinner));
        copyStudentAddressButton.setOnClickListener(v -> copyStudentAddress());
        copyStudentPhoneNumber.setOnClickListener(v -> copyStudentPhoneNumber());
        copyStudentMobilePhoneNumber.setOnClickListener(v -> copyStudentMobilePhoneNumber());
        copyStudentAnotherPhoneNumber.setOnClickListener(v -> copyStudentAnotherPhoneNumber());
        personGendersViewModel.getAll().observe(getActivity(), new Observer<List<PersonGenders>>() {
            @Override
            public void onChanged(@Nullable List<PersonGenders> personGenders) {
                if (personGenders.size() > 0) {
                    personGendersString = stream(personGenders).select(PersonGenders::getDescription).toList().toArray(new String[personGenders.size()]);
                    personGendersList = personGenders;
                    ((SwitchMultiButton) rootView.findViewById(R.id.gender)).setText(personGendersString);
                    ((SwitchMultiButton) rootView.findViewById(R.id.gender)).clearSelection();
                    loadRelationShip();
                } else {
                    finishLoadEvent("No se cargó correctamente el catalogo de sexos de persona.");
                }
            }
        });

        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String dni = s.toString();
                personViewModel.getByDni(dni).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<Person>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(Person person) {
                        if (person != null) {
                            ((EditText) rootView.findViewById(R.id.input_firstame)).setText(person.getFirstName());
                            ((EditText) rootView.findViewById(R.id.input_lastname)).setText(person.getLastName());
                            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                            if (person.getBirthdate() != null) {
                                ((EditText) rootView.findViewById(R.id.input_birthdate)).setText(format.format(person.getBirthdate()));
                            }
                            PersonGenders pg = stream(personGendersList).where(e -> e.getId() == person.getPersonGenderId()).firstOrNull();
                            int pos = 0;
                            if (pg != null) {
                                pos = personGendersList.indexOf(pg);
                            }
                            ((SwitchMultiButton) rootView.findViewById(R.id.gender)).setSelectedTab(pos);

                            ((EditText) rootView.findViewById(R.id.input_phone_number)).setText(person.getPhoneNumber());
                            ((EditText) rootView.findViewById(R.id.input_mobile_phone_number)).setText(person.getMobilePhoneNumber());
                            ((EditText) rootView.findViewById(R.id.input_another_phone_number)).setText(person.getAnotherContactPhone());
                            ((EditText) rootView.findViewById(R.id.input_address)).setText(person.getAddress());
                            ((EditText) rootView.findViewById(R.id.input_location)).setText(person.getLocation());
                            ((EditText) rootView.findViewById(R.id.input_nacionality)).setText(person.getNationality());

                            // buscar datos del tutor
                            searchTutorData(person.getId());

                            tutorId = person.getId();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
            }
        };

        ((EditText) rootView.findViewById(R.id.input_document_number)).addTextChangedListener(textWatcher);

        return rootView;
    }

    private void copyStudentAddress() {
        new MaterialDialog.Builder(getActivity())
                .title("Copiar")
                .content("¿Utilizar domicilio del alumno?")
                .positiveText("Si")
                .negativeText("No")
                .onPositive((d, which) -> {
                    try {
                        Fragment page = getActivity().getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.container + ":0");
                        if (page instanceof NewStudentFragment) {
                            StudentData studentData = ((NewStudentFragment) page).getData();
                            if(studentData.getAddress() != null){
                                ((EditText) rootView.findViewById(R.id.input_address)).setText(studentData.getAddress());
                                ((EditText) rootView.findViewById(R.id.input_location)).setText(studentData.getLocation());
                            }
                        }
                    }catch (Exception e){

                    }
                    d.dismiss();
                })
                .onNegative((d, which) -> d.dismiss())
                .show();
    }

    private void copyStudentPhoneNumber() {
        new MaterialDialog.Builder(getActivity())
                .title("Copiar")
                .content("¿Utilizar Nro. de Teléfono del alumno?")
                .positiveText("Si")
                .negativeText("No")
                .onPositive((d, which) -> {
                    try {
                        Fragment page = getActivity().getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.container + ":0");
                        if (page instanceof NewStudentFragment) {
                            StudentData studentData = ((NewStudentFragment) page).getData();
                            if(studentData.getPhoneNumber() != null){
                                ((EditText) rootView.findViewById(R.id.input_phone_number)).setText(studentData.getPhoneNumber());
                            }
                        }
                    }catch (Exception e){

                    }
                    d.dismiss();
                })
                .onNegative((d, which) -> d.dismiss())
                .show();
    }

    private void copyStudentMobilePhoneNumber() {
        new MaterialDialog.Builder(getActivity())
                .title("Copiar")
                .content("¿Utilizar Nro. de Celular del alumno?")
                .positiveText("Si")
                .negativeText("No")
                .onPositive((d, which) -> {
                    try {
                        Fragment page = getActivity().getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.container + ":0");
                        if (page instanceof NewStudentFragment) {
                            StudentData studentData = ((NewStudentFragment) page).getData();
                            if(studentData.getMobilePhoneNumber() != null){
                                ((EditText) rootView.findViewById(R.id.input_mobile_phone_number)).setText(studentData.getMobilePhoneNumber());
                            }
                        }
                    }catch (Exception e){

                    }
                    d.dismiss();
                })
                .onNegative((d, which) -> d.dismiss())
                .show();
    }

    private void copyStudentAnotherPhoneNumber() {
        new MaterialDialog.Builder(getActivity())
                .title("Copiar")
                .content("¿Utilizar Otro Teléfono de contácto del alumno?")
                .positiveText("Si")
                .negativeText("No")
                .onPositive((d, which) -> {
                    try {
                        Fragment page = getActivity().getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.container + ":0");
                        if (page instanceof NewStudentFragment) {
                            StudentData studentData = ((NewStudentFragment) page).getData();
                            if(studentData.getAnotherContactPhone() != null){
                                ((EditText) rootView.findViewById(R.id.input_another_phone_number)).setText(studentData.getAnotherContactPhone());
                            }
                        }
                    }catch (Exception e){

                    }
                    d.dismiss();
                })
                .onNegative((d, which) -> d.dismiss())
                .show();
    }

    private void loadRelationShip() {
        relationshipViewModel.getAll().observe(getActivity(), new Observer<List<RelationshipData>>() {
            @Override
            public void onChanged(@Nullable List<RelationshipData> relationships) {
                if (relationships.size() > 0) {
                    ((SwitchMultiButton) rootView.findViewById(R.id.relationship_switch)).setText("Madre", "Padre", "Tutor");
                    Spinner spinner2 = rootView.findViewById(R.id.relationship_select);
                    RelationshipData r = new RelationshipData();
                    r.setDescription("Seleccione una opcion");
                    r.setId(-1);
                    relationships.add(0, r);
                    ArrayAdapter<RelationshipData> dataAdapter = new ArrayAdapter<>(getActivity(),
                            android.R.layout.simple_spinner_item, relationships);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner2.setAdapter(dataAdapter);
                    NewTutorFragment.this.relationships = relationships;

                    loadData();
                } else {
                    finishLoadEvent("No se cargó correctamente el catalogo de parentescos.");
                }
            }
        });
    }

    private void loadData() {
        // load data
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            tutorId = bundle.getString("tutorId");
            studentId = bundle.getString("studentId");
        }
        if (tutorId != null && !tutorId.isEmpty()) { //buscar profesor
            tutorViewModel.getTutorData(tutorId, studentId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new MaybeObserver<TutorData>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onSuccess(TutorData tutorData) {
                    // success
                    ((EditText) rootView.findViewById(R.id.input_firstame)).setText(tutorData.getFirstName());
                    ((EditText) rootView.findViewById(R.id.input_lastname)).setText(tutorData.getLastName());
                    ((EditText) rootView.findViewById(R.id.input_document_number)).removeTextChangedListener(textWatcher);
                    ((EditText) rootView.findViewById(R.id.input_document_number)).setText(tutorData.getDocumentNumber());
                    ((EditText) rootView.findViewById(R.id.input_document_number)).addTextChangedListener(textWatcher);
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    if (tutorData.getBirthdate() != null) {
                        ((EditText) rootView.findViewById(R.id.input_birthdate)).setText(format.format(tutorData.getBirthdate()));
                    }
                    PersonGenders pg = stream(personGendersList).where(e -> e.getId() == tutorData.getPersonGenderId()).firstOrNull();
                    int pos = 0;
                    if (pg != null) {
                        pos = personGendersList.indexOf(pg);
                    }
                    ((SwitchMultiButton) rootView.findViewById(R.id.gender)).setSelectedTab(pos);

                    ((EditText) rootView.findViewById(R.id.input_phone_number)).setText(tutorData.getPhoneNumber());
                    ((EditText) rootView.findViewById(R.id.input_mobile_phone_number)).setText(tutorData.getMobilePhoneNumber());
                    ((EditText) rootView.findViewById(R.id.input_another_phone_number)).setText(tutorData.getAnotherContactPhone());
                    ((EditText) rootView.findViewById(R.id.input_address)).setText(tutorData.getAddress());
                    ((EditText) rootView.findViewById(R.id.input_location)).setText(tutorData.getLocation());
                    ((EditText) rootView.findViewById(R.id.input_nacionality)).setText(tutorData.getNationality());

                    ((EditText) rootView.findViewById(R.id.input_ocupation)).setText(tutorData.getOcupation());

                    RelationshipData rd = stream(NewTutorFragment.this.relationships).where(e -> e.getId() == tutorData.getRelationshipId()).firstOrNull();
                    pos = 0;
                    if (rd != null) {
                        pos = NewTutorFragment.this.relationships.indexOf(rd);
                    }
                    ((Spinner) rootView.findViewById(R.id.relationship_select)).setSelection(pos);
                    finishLoadEvent("");
                }

                @Override
                public void onError(Throwable e) {
                    finishLoadEvent("Ocurrió un error al cargar el tutor");
                }

                @Override
                public void onComplete() {

                }
            });
        } else {
            finishLoadEvent("");
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SCANNER_ACTIVITY_RESULT) {
            if (resultCode == Activity.RESULT_OK) {
                Object object = data.getExtras().getSerializable("result");
                String rawResult = data.getExtras().getString("rawResult");
                this.scannerResult = rawResult != null ? rawResult : "";
                if (object instanceof Person) {
                    Person result = (Person) data.getExtras().getSerializable("result");
                    try {
                        ((EditText) rootView.findViewById(R.id.input_firstame)).setText(result.getFirstName());
                        ((EditText) rootView.findViewById(R.id.input_lastname)).setText(result.getLastName());
                        ((EditText) rootView.findViewById(R.id.input_document_number)).removeTextChangedListener(textWatcher);
                        ((EditText) rootView.findViewById(R.id.input_document_number)).setText(result.getDocumentNumber());
                        ((EditText) rootView.findViewById(R.id.input_document_number)).addTextChangedListener(textWatcher);
                        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                        ((EditText) rootView.findViewById(R.id.input_birthdate)).setText(format.format(result.getBirthdate()));
                        ((SwitchMultiButton) rootView.findViewById(R.id.gender)).setSelectedTab(result.getPersonGenderId() - 1);

                        // buscar la persona y traer los datos no escaneados por el dni (Siempre tomar los datos del dni excepto los que no se escanean)
                        personViewModel.getByDni(result.getDocumentNumber()).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<Person>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onSuccess(Person person) {
                                if (person != null) {
                                    ((EditText) rootView.findViewById(R.id.input_phone_number)).setText(person.getPhoneNumber());
                                    ((EditText) rootView.findViewById(R.id.input_mobile_phone_number)).setText(person.getMobilePhoneNumber());
                                    ((EditText) rootView.findViewById(R.id.input_another_phone_number)).setText(person.getAnotherContactPhone());
                                    ((EditText) rootView.findViewById(R.id.input_address)).setText(person.getAddress());
                                    ((EditText) rootView.findViewById(R.id.input_location)).setText(person.getLocation());
                                    ((EditText) rootView.findViewById(R.id.input_nacionality)).setText(person.getNationality());

                                    // buscar datos del tutor
                                    searchTutorData(person.getId());
                                }
                            }

                            @Override
                            public void onError(Throwable e) {

                            }
                        });

                    } catch (Exception e) {
                        new MaterialDialog.Builder(getActivity())
                                .title("Error")
                                .content("Posible error al mostrar los datos escaneados. Por favor verifique si son correctos. Caso contrario intentelo nuevamente.")
                                .positiveText("Aceptar")
                                .onPositive((d, which) -> d.dismiss())
                                .show();
                    }
                } else {
                    new MaterialDialog.Builder(getActivity())
                            .title("Error")
                            .content("Hubo un error al escanear los datos. Intentelo nuevamente.")
                            .positiveText("Aceptar")
                            .onPositive((d, which) -> d.dismiss())
                            .show();
                }
            }
        }
    }

    private void searchTutorData(String tutorId) {
        tutorViewModel.getTutor(tutorId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new MaybeObserver<Tutor>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(Tutor tutorData) {
                // success
                ((EditText) rootView.findViewById(R.id.input_ocupation)).setText(tutorData.getOcupation());
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        dateTextView.setText(simpleDateFormat.format(calendar.getTime()));
    }

    void showDate(int year, int monthOfYear, int dayOfMonth, int spinnerTheme) {
        String bd = ((EditText) rootView.findViewById(R.id.input_birthdate)).getText().toString().trim();
        Date date;
        if (bd.isEmpty()) {
            date = new Date();
        } else {
            try {
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                date = format.parse(bd);
            } catch (Exception ignored) {
                date = new Date();
            }
        }
        new SpinnerDatePickerDialogBuilder()
                .context(getActivity())
                .callback(NewTutorFragment.this)
                .spinnerTheme(spinnerTheme)
                .defaultDate(Integer.parseInt(DateFormat.format("yyyy", date).toString()), Integer.parseInt(DateFormat.format("M", date).toString()) - 1, Integer.parseInt(DateFormat.format("d", date).toString()))
                .build()
                .show();
    }

    public void finishLoadEvent(String event) {
        EventBus.getDefault().post(new Evento("finishLoadData", event));
    }

    public void callScanner() {
        Intent i = new Intent(getActivity(), SimpleScannerActivity.class);
        startActivityForResult(i, SCANNER_ACTIVITY_RESULT);
    }

    public TutorData getData() {
        TutorData tutorData = new TutorData();
        tutorData.setId(tutorId);
        tutorData.setFirstName(((EditText) rootView.findViewById(R.id.input_firstame)).getText().toString());
        tutorData.setLastName(((EditText) rootView.findViewById(R.id.input_lastname)).getText().toString());
        tutorData.setDocumentNumber(((EditText) rootView.findViewById(R.id.input_document_number)).getText().toString());


        int posGender = ((SwitchMultiButton) rootView.findViewById(R.id.gender)).getSelectedTab();
        if (posGender != -1) {
            PersonGenders personGenders = personGendersList.get(posGender);
            tutorData.setPersonGenderId(personGenders.getId());
        } else {
            tutorData.setPersonGenderId(posGender);
        }

        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            String bd = ((EditText) rootView.findViewById(R.id.input_birthdate)).getText().toString();
            tutorData.setBirthdate(format.parse(bd));
        } catch (Exception ignored) {
        }

        tutorData.setPhoneNumber(((EditText) rootView.findViewById(R.id.input_phone_number)).getText().toString());
        tutorData.setMobilePhoneNumber(((EditText) rootView.findViewById(R.id.input_mobile_phone_number)).getText().toString());
        tutorData.setAnotherContactPhone(((EditText) rootView.findViewById(R.id.input_another_phone_number)).getText().toString());
        tutorData.setAddress(((EditText) rootView.findViewById(R.id.input_address)).getText().toString());
        tutorData.setLocation(((EditText) rootView.findViewById(R.id.input_location)).getText().toString());
        tutorData.setNationality(((EditText) rootView.findViewById(R.id.input_nacionality)).getText().toString());
        tutorData.setOcupation(((EditText) rootView.findViewById(R.id.input_ocupation)).getText().toString());

        RelationshipData relationshipData = (RelationshipData) ((Spinner) rootView.findViewById(R.id.relationship_select)).getSelectedItem();
        tutorData.setRelationshipId(relationshipData.getId());

        tutorData.setScannerResult(this.scannerResult);

        return tutorData;
    }


    public void disableComponents() {
        studentId = null;
        ((EditText) rootView.findViewById(R.id.input_firstame)).setEnabled(false);
        ((EditText) rootView.findViewById(R.id.input_lastname)).setEnabled(false);
        ((EditText) rootView.findViewById(R.id.input_document_number)).setEnabled(false);
        ((SwitchMultiButton) rootView.findViewById(R.id.gender)).setEnabled(false);
        ((EditText) rootView.findViewById(R.id.input_birthdate)).setEnabled(false);
        ((EditText) rootView.findViewById(R.id.input_phone_number)).setEnabled(false);
        ((EditText) rootView.findViewById(R.id.input_mobile_phone_number)).setEnabled(false);
        ((EditText) rootView.findViewById(R.id.input_another_phone_number)).setEnabled(false);
        ((EditText) rootView.findViewById(R.id.input_address)).setEnabled(false);
        ((EditText) rootView.findViewById(R.id.input_location)).setEnabled(false);
        ((EditText) rootView.findViewById(R.id.input_nacionality)).setEnabled(false);
        ;
        ((SwitchMultiButton) rootView.findViewById(R.id.curso_switch)).setEnabled(false);
    }

    public void setTutorId(String tutorId) {
        this.tutorId = tutorId;
    }
}
