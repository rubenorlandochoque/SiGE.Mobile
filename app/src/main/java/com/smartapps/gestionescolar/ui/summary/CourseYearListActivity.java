package com.smartapps.gestionescolar.ui.summary;

import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.smartapps.gestionescolar.R;
import com.smartapps.gestionescolar.classes.SharedConfig;
import com.smartapps.gestionescolar.models.courseyear.CourseYearViewModel;
import com.smartapps.gestionescolar.models.courseyear.pojo.CourseYearStudentsCount;
import com.smartapps.gestionescolar.models.students.StudentViewModel;
import com.smartapps.gestionescolar.models.teacher.TeacherViewModel;
import com.smartapps.gestionescolar.services.Upload;
import com.smartapps.gestionescolar.ui.newstudent.StudentTutorActivity;

import java.util.List;

public class CourseYearListActivity extends AppCompatActivity implements View.OnClickListener {
    private CourseYearViewModel courseYearViewModel;

    private ProgressDialog dialog;
    private StudentViewModel studentViewModel;
    private TeacherViewModel teacherViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_year_list);

        studentViewModel = ViewModelProviders.of(this).get(StudentViewModel.class);
        teacherViewModel = ViewModelProviders.of(this).get(TeacherViewModel.class);

        String shit = SharedConfig.getShiftDescription();
        ((TextView) findViewById(R.id.txt_ab_title)).setText(String.format("Turno: %s", shit));

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        final CourseYearListAdapter adapter = new CourseYearListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        findViewById(R.id.newStudent).setOnClickListener(this);

        courseYearViewModel = ViewModelProviders.of(this).get(CourseYearViewModel.class);
        courseYearViewModel.getAllWithStudentsCount(SharedConfig.getShiftId()).observe(CourseYearListActivity.this, new Observer<List<CourseYearStudentsCount>>() {
            @Override
            public void onChanged(@Nullable List<CourseYearStudentsCount> courseYears) {
                adapter.setCourseYears(courseYears);
                adapter.notifyDataSetChanged();
            }
        });

        findViewById(R.id.sync_button).setVisibility(View.VISIBLE);
        findViewById(R.id.sync_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((dialog == null || !dialog.isShowing())) {
                    dialog = ProgressDialog.show(CourseYearListActivity.this, "",
                            "" +
                                    "Subiendo datos", true);
                }
                new Upload().Sync(studentViewModel, teacherViewModel, new Upload.UploadListner() {
                    @Override
                    public void finish(boolean successImages, boolean successStudent, boolean successTeacher) {
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        if (successImages && successStudent && successTeacher) {
                            new MaterialDialog.Builder(CourseYearListActivity.this)
                                    .title("Sincronización completa")
                                    .content("Los datos se sincronizaron correctamente")
                                    .positiveText("Aceptar")
                                    .onPositive((d, which) -> {
                                        d.dismiss();
                                    })
                                    .show();
                        } else {
                            new MaterialDialog.Builder(CourseYearListActivity.this)
                                    .title("Error al sincronizar")
                                    .content("Hubo un error al sincronizar los datos.\nPor favor intente nuevamente.")
                                    .positiveText("Aceptar")
                                    .onPositive((d, which) -> {
                                        d.dismiss();
                                    })
                                    .show();
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.newStudent: {
                Intent intent = new Intent(CourseYearListActivity.this, StudentTutorActivity.class);
                startActivity(intent);
                break;
            }
        }
    }
}
