package com.smartapps.gestionescolar.ui.summaryteacher;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.smartapps.gestionescolar.R;
import com.smartapps.gestionescolar.classes.SharedConfig;
import com.smartapps.gestionescolar.models.teacher.TeacherViewModel;
import com.smartapps.gestionescolar.models.teacher.pojo.TeacherListData;
import com.smartapps.gestionescolar.ui.teacher.TeacherActivity;

import java.util.List;

public class TeacherListActivity extends AppCompatActivity implements View.OnClickListener {
    private TeacherViewModel teacherViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_list);


        RecyclerView recyclerView = findViewById(R.id.recyclerview_teacher_list);
        final TeacherListAdapter adapter = new TeacherListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        findViewById(R.id.newTeacher).setOnClickListener(this);

        teacherViewModel = ViewModelProviders.of(this).get(TeacherViewModel.class);
        teacherViewModel.getTeacherList(SharedConfig.getShiftId()).observe(TeacherListActivity.this, new Observer<List<TeacherListData>>() {
            @Override
            public void onChanged(@Nullable List<TeacherListData> teacherListData) {
                adapter.setTeacherListData(teacherListData);
                adapter.notifyDataSetChanged();
                if (teacherListData.size() > 0) {
                    findViewById(R.id.recyclerview_teacher_list).setVisibility(View.VISIBLE);
                    findViewById(R.id.empty_view).setVisibility(View.GONE);
                } else {
                    findViewById(R.id.recyclerview_teacher_list).setVisibility(View.GONE);
                    findViewById(R.id.empty_view).setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.newTeacher: {
                Intent intent = new Intent(TeacherListActivity.this, TeacherActivity.class);
                startActivity(intent);
                break;
            }
        }
    }
}
