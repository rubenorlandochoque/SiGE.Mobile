package com.smartapps.gestionescolar.ui.newstudent;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.smartapps.gestionescolar.R;
import com.smartapps.gestionescolar.classes.Config;
import com.smartapps.gestionescolar.classes.SharedConfig;
import com.smartapps.gestionescolar.models.courseyear.CourseYearViewModel;
import com.smartapps.gestionescolar.models.courseyear.pojo.CourseYearAndAllDivisions;
import com.smartapps.gestionescolar.models.division.Division;
import com.smartapps.gestionescolar.models.documentations.Documentation;
import com.smartapps.gestionescolar.models.documentations.DocumentationViewModel;
import com.smartapps.gestionescolar.models.people.Person;
import com.smartapps.gestionescolar.models.people.PersonViewModel;
import com.smartapps.gestionescolar.models.persongenders.PersonGenders;
import com.smartapps.gestionescolar.models.persongenders.PersonGendersViewModel;
import com.smartapps.gestionescolar.models.studentdocumentations.StudentDocumentationViewModel;
import com.smartapps.gestionescolar.models.students.StudentViewModel;
import com.smartapps.gestionescolar.models.students.pojo.StudentData;
import com.smartapps.gestionescolar.ui.fullscrenpreview.FullScreenImageActivity;
import com.smartapps.gestionescolar.ui.multimedia.CustomCameraActivity;
import com.smartapps.gestionescolar.ui.scanner.SimpleScannerActivity;
import com.smartapps.gestionescolar.util.Evento;
import com.smartapps.gestionescolar.util.Util;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import io.reactivex.MaybeObserver;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lib.kingja.switchbutton.SwitchMultiButton;

import static br.com.zbra.androidlinq.Linq.stream;

public class NewStudentFragment extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    public static final int SCANNER_ACTIVITY_RESULT = 2;
    private static final int CUSTOM_CAMERA_REQUEST_CODE_ANVERSO = 1000;
    private static final int CUSTOM_CAMERA_REQUEST_CODE_REVERSO = 1001;
    private StudentViewModel studentViewModel;
    private CourseYearViewModel courseYearViewModel;
    private PersonGendersViewModel personGendersViewModel;
    private StudentDocumentationViewModel studentDocumentationViewModel;
    private PersonViewModel personViewModel;
    private DocumentationViewModel documentationViewModel;
    TextWatcher textWatcher;

    List<CustomCourseYear> courseYearsList;
    List<Division> divisionsList;
    List<PersonGenders> personGendersList;
    String[] courseYearString;
    String[] divisionsString;
    String[] personGendersString;
    TextView dateTextView;
    Button dateButton;
    SimpleDateFormat simpleDateFormat;
    View rootView;
    String studentId = "";
    int courseYearId = 0;
    String idCardFront = "";
    String idCardBack = "";
    String scannerResult = "";

    public NewStudentFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // bind wiews models
        courseYearViewModel = ViewModelProviders.of(this).get(CourseYearViewModel.class);
        studentViewModel = ViewModelProviders.of(this).get(StudentViewModel.class);
        personGendersViewModel = ViewModelProviders.of(this).get(PersonGendersViewModel.class);
        documentationViewModel = ViewModelProviders.of(this).get(DocumentationViewModel.class);
        personViewModel = ViewModelProviders.of(this).get(PersonViewModel.class);
        studentDocumentationViewModel = ViewModelProviders.of(this).get(StudentDocumentationViewModel.class);

        rootView = inflater.inflate(R.layout.fragment_new_student, container, false);
        ((EditText) rootView.findViewById(R.id.input_lastname)).setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        ((EditText) rootView.findViewById(R.id.input_firstame)).setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        rootView.findViewById(R.id.input_layout_age).setVisibility(View.VISIBLE);
        rootView.findViewById(R.id.input_layout_another_phone_number).setVisibility(View.VISIBLE);
        dateButton = rootView.findViewById(R.id.set_date_button);
        dateTextView = rootView.findViewById(R.id.input_birthdate);
        simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        dateButton.setOnClickListener(view -> showDate(1980, 0, 1, R.style.DatePickerSpinner));

        rootView.findViewById(R.id.input_layout_belongs_ethnic_group_observation).setVisibility(View.GONE);
        rootView.findViewById(R.id.input_layout_has_health_problems_observation).setVisibility(View.GONE);
        rootView.findViewById(R.id.input_layout_has_legal_problems_observation).setVisibility(View.GONE);

        ((SwitchMultiButton) rootView.findViewById(R.id.has_brother_switch)).clearSelection();
        ((SwitchMultiButton) rootView.findViewById(R.id.belongs_ethnic_group)).clearSelection();
        ((SwitchMultiButton) rootView.findViewById(R.id.has_health_problems)).clearSelection();
        ((SwitchMultiButton) rootView.findViewById(R.id.has_legal_problems)).clearSelection();

        ((SwitchMultiButton) rootView.findViewById(R.id.belongs_ethnic_group)).setOnSwitchListener((position, tabText) -> {
            if(position == 0) {
                rootView.findViewById(R.id.input_layout_belongs_ethnic_group_observation).setVisibility(View.VISIBLE);
            }else{
                rootView.findViewById(R.id.input_layout_belongs_ethnic_group_observation).setVisibility(View.GONE);
            }
        });

        ((SwitchMultiButton) rootView.findViewById(R.id.has_health_problems)).setOnSwitchListener((position, tabText) -> {
            if(position == 0) {
                rootView.findViewById(R.id.input_layout_has_health_problems_observation).setVisibility(View.VISIBLE);
            }else{
                rootView.findViewById(R.id.input_layout_has_health_problems_observation).setVisibility(View.GONE);
            }
        });

        ((SwitchMultiButton) rootView.findViewById(R.id.has_legal_problems)).setOnSwitchListener((position, tabText) -> {
            if(position == 0) {
                rootView.findViewById(R.id.input_layout_has_legal_problems_observation).setVisibility(View.VISIBLE);
            }else{
                rootView.findViewById(R.id.input_layout_has_legal_problems_observation).setVisibility(View.GONE);
            }
        });

        loadDocumentations();

        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String dni = s.toString();
                personViewModel.getByDni(dni).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<Person>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(Person person) {
                        if (person != null) {
                            ((EditText) rootView.findViewById(R.id.input_firstame)).setText(person.getFirstName());
                            ((EditText) rootView.findViewById(R.id.input_lastname)).setText(person.getLastName());
                            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                            if (person.getBirthdate() != null) {
                                ((EditText) rootView.findViewById(R.id.input_birthdate)).setText(format.format(person.getBirthdate()));
                            }
                            PersonGenders pg = stream(personGendersList).where(e -> e.getId() == person.getPersonGenderId()).firstOrNull();
                            int pos = 0;
                            if (pg != null) {
                                pos = personGendersList.indexOf(pg);
                            }
                            ((SwitchMultiButton) rootView.findViewById(R.id.gender)).setSelectedTab(pos);

                            ((EditText) rootView.findViewById(R.id.input_phone_number)).setText(person.getPhoneNumber());
                            ((EditText) rootView.findViewById(R.id.input_mobile_phone_number)).setText(person.getMobilePhoneNumber());
                            ((EditText) rootView.findViewById(R.id.input_another_phone_number)).setText(person.getAnotherContactPhone());
                            ((EditText) rootView.findViewById(R.id.input_address)).setText(person.getAddress());
                            ((EditText) rootView.findViewById(R.id.input_location)).setText(person.getLocation());
                            ((EditText) rootView.findViewById(R.id.input_nacionality)).setText(person.getNationality());

                            int beg = person.isBelongsEthnicGroup() ? 0 : 1;
                            ((SwitchMultiButton) rootView.findViewById(R.id.belongs_ethnic_group)).setSelectedTab(beg);

                            int hhp = person.isHasHealthProblems() ? 0 : 1;
                            ((SwitchMultiButton) rootView.findViewById(R.id.has_health_problems)).setSelectedTab(hhp);

                            int hlp = person.isHasLegalProblems() ? 0 : 1;
                            ((SwitchMultiButton) rootView.findViewById(R.id.has_legal_problems)).setSelectedTab(hlp);


                            if(person.isBelongsEthnicGroup()){
                                ((EditText) rootView.findViewById(R.id.input_belongs_ethnic_group_observation)).setText(person.getEthnicName());
                            }
                            if(person.isHasHealthProblems()){
                                ((EditText) rootView.findViewById(R.id.input_has_health_problems_observation)).setText(person.getDescriptionHealthProblems());
                            }
                            if(person.isHasLegalProblems()){
                                ((EditText) rootView.findViewById(R.id.input_has_legal_problems_observation)).setText(person.getDescriptionLegalProblems());
                            }
                            // buscar datos como alumno
                            searchStudentData(person.getId());

                            studentId = person.getId();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
            }
        };

        ((EditText) rootView.findViewById(R.id.input_document_number)).addTextChangedListener(textWatcher);

        rootView.findViewById(R.id.img_add_anverso_dni).setOnClickListener(v -> {
            Intent i = new Intent(getActivity(), CustomCameraActivity.class);
            i.putExtra("directorioFoto", Config.DIRECTORIO_FOTOS);
            Config.BANDERA_FOTO = true; // Indica que se está sacando una foto.
            startActivityForResult(i, CUSTOM_CAMERA_REQUEST_CODE_ANVERSO);
        });

        rootView.findViewById(R.id.img_add_reverso_dni).setOnClickListener(v -> {
            Intent i = new Intent(getActivity(), CustomCameraActivity.class);
            i.putExtra("directorioFoto", Config.DIRECTORIO_FOTOS);
            Config.BANDERA_FOTO = true; // Indica que se está sacando una foto.
            startActivityForResult(i, CUSTOM_CAMERA_REQUEST_CODE_REVERSO);
        });

        rootView.findViewById(R.id.img_anverso_dni).setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), FullScreenImageActivity.class);
            if (!idCardFront.isEmpty()) {
                intent.putExtra("path", idCardFront);
                startActivity(intent);
            }
        });

        rootView.findViewById(R.id.img_reverso_dni).setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), FullScreenImageActivity.class);
            if (!idCardBack.isEmpty()) {
                intent.putExtra("path", idCardBack);
                startActivity(intent);
            }
        });

        return rootView;
    }

    private void loadDocumentations(){
        documentationViewModel.getAll().observe(getActivity(), new Observer<List<Documentation>>() {
            @Override
            public void onChanged(@Nullable List<Documentation> documentations) {
                if (documentations.size() > 0) {
                    LinearLayout documentationsContainer = rootView.findViewById(R.id.documentations_container);
                    for(Documentation documentation: documentations){
                        CheckBox cb = new CheckBox(getActivity());
                        cb.setText(Html.fromHtml(documentation.getDescription()));
                        cb.setTag(documentation.getId());
                        documentationsContainer.addView(cb);
                    }
                    loadPersonGenders();
                } else {
                    finishLoadEvent("No se cargó correctamente el catalogo de documentación.");
                }
            }
        });
    }

    private void loadPersonGenders(){
        personGendersViewModel.getAll().observe(getActivity(), new Observer<List<PersonGenders>>() {
            @Override
            public void onChanged(@Nullable List<PersonGenders> personGenders) {
                if (personGenders.size() > 0) {
                    personGendersString = stream(personGenders).select(PersonGenders::getDescription).toList().toArray(new String[personGenders.size()]);
                    personGendersList = personGenders;
                    ((SwitchMultiButton) rootView.findViewById(R.id.gender)).setText(personGendersString);
                    ((SwitchMultiButton) rootView.findViewById(R.id.gender)).clearSelection();
                    loadCourseDivisions();
                } else {
                    finishLoadEvent("No se cargó correctamente el catalogo de sexos de persona.");
                }
            }
        });
    }

    private void loadCourseDivisions() {
        courseYearViewModel.getCourseYearsDivisionsByShiftId(SharedConfig.getShiftId()).observe(getActivity(), new Observer<List<CourseYearAndAllDivisions>>() {
            @Override
            public void onChanged(@Nullable List<CourseYearAndAllDivisions> courseYears) {
                if (courseYears.size() > 0) {
                    // agrupar las divisiones por curso, refactorizar esta parte tratando de usar consultas de ROOM
                    List<CustomCourseYear> ccy = new ArrayList<>();
                    for (CourseYearAndAllDivisions cyd : courseYears) {
                        CustomCourseYear customCourseYear = stream(ccy).firstOrNull(e -> e.Id == cyd.CourseYearId);
                        if (customCourseYear == null) {
                            customCourseYear = new CustomCourseYear(cyd.CourseYearId, cyd.CourseYearName);
                            ccy.add(customCourseYear);
                        }
                        customCourseYear.divisions.add(new CustomDivision(cyd.DivisionId, cyd.DivisionName));
                    }
                    // fin agrupamiento

                    courseYearString = stream(ccy).select(c -> c.CourseYearName).toList().toArray(new String[ccy.size()]);
                    courseYearsList = ccy;
                    ((SwitchMultiButton) rootView.findViewById(R.id.curso_switch)).setText(courseYearString);
                    ((SwitchMultiButton) rootView.findViewById(R.id.curso_switch)).clearSelection();
                    ((SwitchMultiButton) rootView.findViewById(R.id.curso_switch)).setOnSwitchListener(new SwitchMultiButton.OnSwitchListener() {
                        @Override
                        public void onSwitch(int position, String tabText) {
                            CustomCourseYear c = courseYearsList.get(position);
                            divisionsString = stream(c.divisions).select(e -> e.DivisionName).toList().toArray(new String[c.divisions.size()]);
                            if (divisionsString.length > 0) {
                                if (divisionsString.length > 1) {
                                    rootView.findViewById(R.id.division_switch).setVisibility(View.VISIBLE);
                                    rootView.findViewById(R.id.unique_division).setVisibility(View.GONE);
                                    ((SwitchMultiButton) rootView.findViewById(R.id.division_switch)).setText(divisionsString);
                                    ((SwitchMultiButton) rootView.findViewById(R.id.division_switch)).clearSelection();
                                } else {
                                    rootView.findViewById(R.id.division_switch).setVisibility(View.GONE);
                                    rootView.findViewById(R.id.unique_division).setVisibility(View.VISIBLE);
                                    ((TextView) rootView.findViewById(R.id.unique_division)).setText(divisionsString[0]);
                                }
                            } else {
                                finishLoadEvent("No existen Cursos para asignar.");
                            }
                        }
                    });
                    rootView.findViewById(R.id.unique_division).setVisibility(View.GONE);
                    rootView.findViewById(R.id.division_switch).setVisibility(View.GONE);
                    loadStudentData();
                } else {
                    finishLoadEvent("No existen Cursos para asignar.");
                }
            }
        });
    }

    private void loadStudentData() {
        // load data
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            studentId = bundle.getString("studentId");
        }
        if (studentId != null && !studentId.isEmpty()) { //buscar estudiante
            studentViewModel.getStudent(studentId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new MaybeObserver<StudentData>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onSuccess(StudentData studentData) {
                    // success
                    ((EditText) rootView.findViewById(R.id.input_firstame)).setText(studentData.getFirstName());
                    ((EditText) rootView.findViewById(R.id.input_lastname)).setText(studentData.getLastName());
                    ((EditText) rootView.findViewById(R.id.input_document_number)).removeTextChangedListener(textWatcher);
                    ((EditText) rootView.findViewById(R.id.input_document_number)).setText(studentData.getDocumentNumber());
                    ((EditText) rootView.findViewById(R.id.input_document_number)).addTextChangedListener(textWatcher);
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    if (studentData.getBirthdate() != null) {
                        ((EditText) rootView.findViewById(R.id.input_birthdate)).setText(format.format(studentData.getBirthdate()));
                    }

                    PersonGenders pg = stream(personGendersList).where(e -> e.getId() == studentData.getPersonGenderId()).firstOrNull();
                    int pos = 0;
                    if (pg != null) {
                        pos = personGendersList.indexOf(pg);
                    }
                    ((SwitchMultiButton) rootView.findViewById(R.id.gender)).setSelectedTab(pos);


                    ((EditText) rootView.findViewById(R.id.input_phone_number)).setText(studentData.getPhoneNumber());
                    ((EditText) rootView.findViewById(R.id.input_mobile_phone_number)).setText(studentData.getMobilePhoneNumber());
                    ((EditText) rootView.findViewById(R.id.input_another_phone_number)).setText(studentData.getAnotherContactPhone());
                    ((EditText) rootView.findViewById(R.id.input_address)).setText(studentData.getAddress());
                    ((EditText) rootView.findViewById(R.id.input_location)).setText(studentData.getLocation());
                    ((EditText) rootView.findViewById(R.id.input_nacionality)).setText(studentData.getNationality());

                    ((EditText) rootView.findViewById(R.id.input_school_origin)).setText(studentData.SchoolOrigin);
                    ((EditText) rootView.findViewById(R.id.input_another_phone_number_owner)).setText(studentData.OtherPhoneBelongs);
                    ((EditText) rootView.findViewById(R.id.input_observation)).setText(studentData.Observation);


                    int hb = studentData.HasBrothersOfSchoolAge ? 0 : 1;
                    ((SwitchMultiButton) rootView.findViewById(R.id.has_brother_switch)).setSelectedTab(hb);

                    int beg = studentData.isBelongsEthnicGroup() ? 0 : 1;
                    ((SwitchMultiButton) rootView.findViewById(R.id.belongs_ethnic_group)).setSelectedTab(beg);

                    int hhp = studentData.isHasHealthProblems() ? 0 : 1;
                    ((SwitchMultiButton) rootView.findViewById(R.id.has_health_problems)).setSelectedTab(hhp);

                    int hlp = studentData.isHasLegalProblems() ? 0 : 1;
                    ((SwitchMultiButton) rootView.findViewById(R.id.has_legal_problems)).setSelectedTab(hlp);

                    if(studentData.isBelongsEthnicGroup()){
                        ((EditText) rootView.findViewById(R.id.input_belongs_ethnic_group_observation)).setText(studentData.getEthnicName());
                    }
                    if(studentData.isHasHealthProblems()){
                        ((EditText) rootView.findViewById(R.id.input_has_health_problems_observation)).setText(studentData.getDescriptionHealthProblems());
                    }
                    if(studentData.isHasLegalProblems()){
                        ((EditText) rootView.findViewById(R.id.input_has_legal_problems_observation)).setText(studentData.getDescriptionLegalProblems());
                    }

                    int selectedCourse = 0;
                    int selectedDivision = 0;
                    for (CustomCourseYear cy : courseYearsList) {
                        CustomDivision division = stream(cy.divisions).where(e -> e.Id == studentData.DivisionId).firstOrNull();
                        if (division != null) {
                            selectedCourse = courseYearsList.indexOf(cy);
                            selectedDivision = cy.divisions.indexOf(division);
                            break;
                        }
                    }

                    ((SwitchMultiButton) rootView.findViewById(R.id.curso_switch)).setSelectedTab(selectedCourse);
                    ((SwitchMultiButton) rootView.findViewById(R.id.division_switch)).setSelectedTab(selectedDivision);
                    calcularEdad();
                    idCardBack = studentData.IdCardBack;
                    idCardFront = studentData.IdCardFront;
                    if (idCardFront != null && !idCardFront.isEmpty()) {
                        new LoadImagen(idCardFront, rootView.findViewById(R.id.img_anverso_dni)).execute();
                    }
                    if (idCardBack != null && !idCardBack.isEmpty()) {
                        new LoadImagen(idCardBack, rootView.findViewById(R.id.img_reverso_dni)).execute();
                    }

                    // load documentations
                    studentDocumentationViewModel.getDocumentations(studentId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new MaybeObserver<List<Integer>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onSuccess(List<Integer> documentations) {
                            // success
                            studentData.documentations = documentations;
                            finishLoadEvent("");
                            LinearLayout documentationsContainer = rootView.findViewById(R.id.documentations_container);
                            for(int documentation: studentData.documentations){
                                View v = documentationsContainer.findViewWithTag(documentation);
                                if(v instanceof CheckBox){
                                    CheckBox checkBox = (CheckBox)v;
                                    checkBox.setChecked(true);
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            finishLoadEvent("Ocurrió un error al cargar el alumno");
                        }

                        @Override
                        public void onComplete() {
                            finishLoadEvent("Ocurrió un error al cargar el alumno");
                        }
                    });
                }

                @Override
                public void onError(Throwable e) {
                    finishLoadEvent("Ocurrió un error al cargar el alumno");
                }

                @Override
                public void onComplete() {
                    finishLoadEvent("Ocurrió un error al cargar el alumno");
                }
            });
        } else {
            if (bundle != null) {
                courseYearId = bundle.getInt("courseYearId");
            }
            if (courseYearId != 0) {
                int selectedCourse = -1;
                CustomCourseYear courseYear = stream(courseYearsList).where(e -> e.Id == courseYearId).firstOrNull();
                if (courseYear != null) {
                    selectedCourse = courseYearsList.indexOf(courseYear);
                    ((SwitchMultiButton) rootView.findViewById(R.id.curso_switch)).setSelectedTab(selectedCourse);
                }
            }
            finishLoadEvent("");
        }
    }

    private void calcularEdad() {
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            String bd = ((EditText) rootView.findViewById(R.id.input_birthdate)).getText().toString();
            Calendar calendarFechaNacimiento = Calendar.getInstance();
            calendarFechaNacimiento.setTime(format.parse(bd));
            Calendar calendarFechaActual = Calendar.getInstance();
            calendarFechaActual.setTime(new Date());
            Integer edad = Util.calcularEdad(calendarFechaActual, calendarFechaNacimiento);
            EditText editText = rootView.findViewById(R.id.input_age);
            editText.setText(String.format("%d años", edad));
        } catch (Exception ignored) {

        }
    }

    public void finishLoadEvent(String event) {
        EventBus.getDefault().post(new Evento("finishLoadData", event));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CUSTOM_CAMERA_REQUEST_CODE_ANVERSO:
                    String path = data.getStringExtra("filePath") != null ? data.getStringExtra("filePath") : "";
                    if (!path.isEmpty()) {
                        idCardFront = path;
                        new LoadImagen(path, rootView.findViewById(R.id.img_anverso_dni)).execute();
                    } else {
                        new MaterialDialog.Builder(getActivity())
                                .title("Error")
                                .content("No se pudo registrar la foto correctamente, intentelo nuevamente.")
                                .positiveText("Aceptar")
                                .onPositive((d, which) -> d.dismiss())
                                .show();
                    }
                    break;
                case CUSTOM_CAMERA_REQUEST_CODE_REVERSO:
                    path = data.getStringExtra("filePath") != null ? data.getStringExtra("filePath") : "";
                    if (!path.isEmpty()) {
                        idCardBack = path;
                        new LoadImagen(path, rootView.findViewById(R.id.img_reverso_dni)).execute();
                    } else {
                        new MaterialDialog.Builder(getActivity())
                                .title("Error")
                                .content("No se pudo registrar la foto correctamente, intentelo nuevamente.")
                                .positiveText("Aceptar")
                                .onPositive((d, which) -> d.dismiss())
                                .show();
                    }
                    break;
                case SCANNER_ACTIVITY_RESULT:
                    Object object = data.getExtras().getSerializable("result");
                    String rawResult = data.getExtras().getString("rawResult");
                    this.scannerResult = rawResult != null ? rawResult : "";
                    if (object instanceof Person) {
                        Person result = (Person) data.getExtras().getSerializable("result");
                        try {
                            ((EditText) rootView.findViewById(R.id.input_firstame)).setText(result.getFirstName());
                            ((EditText) rootView.findViewById(R.id.input_lastname)).setText(result.getLastName());
                            ((EditText) rootView.findViewById(R.id.input_document_number)).removeTextChangedListener(textWatcher);
                            ((EditText) rootView.findViewById(R.id.input_document_number)).setText(result.getDocumentNumber());
                            ((EditText) rootView.findViewById(R.id.input_document_number)).addTextChangedListener(textWatcher);
                            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                            ((EditText) rootView.findViewById(R.id.input_birthdate)).setText(format.format(result.getBirthdate()));
                            ((SwitchMultiButton) rootView.findViewById(R.id.gender)).setSelectedTab(result.getPersonGenderId() - 1);
                            calcularEdad();

                            // buscar la persona y traer los datos no escaneados por el dni (Siempre tomar los datos del dni excepto los que no se escanean)
                            personViewModel.getByDni(result.getDocumentNumber()).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<Person>() {
                                @Override
                                public void onSubscribe(Disposable d) {

                                }

                                @Override
                                public void onSuccess(Person person) {
                                    if (person != null) {
                                        ((EditText) rootView.findViewById(R.id.input_phone_number)).setText(person.getPhoneNumber());
                                        ((EditText) rootView.findViewById(R.id.input_mobile_phone_number)).setText(person.getMobilePhoneNumber());
                                        ((EditText) rootView.findViewById(R.id.input_another_phone_number)).setText(person.getAnotherContactPhone());
                                        ((EditText) rootView.findViewById(R.id.input_address)).setText(person.getAddress());
                                        ((EditText) rootView.findViewById(R.id.input_location)).setText(person.getLocation());
                                        ((EditText) rootView.findViewById(R.id.input_nacionality)).setText(person.getNationality());

                                        int beg = person.isBelongsEthnicGroup() ? 0 : 1;
                                        ((SwitchMultiButton) rootView.findViewById(R.id.belongs_ethnic_group)).setSelectedTab(beg);

                                        int hhp = person.isHasHealthProblems() ? 0 : 1;
                                        ((SwitchMultiButton) rootView.findViewById(R.id.has_health_problems)).setSelectedTab(hhp);

                                        int hlp = person.isHasLegalProblems() ? 0 : 1;
                                        ((SwitchMultiButton) rootView.findViewById(R.id.has_legal_problems)).setSelectedTab(hlp);

                                        if(person.isBelongsEthnicGroup()){
                                            ((EditText) rootView.findViewById(R.id.input_belongs_ethnic_group_observation)).setText(person.getEthnicName());
                                        }
                                        if(person.isHasHealthProblems()){
                                            ((EditText) rootView.findViewById(R.id.input_has_health_problems_observation)).setText(person.getDescriptionHealthProblems());
                                        }
                                        if(person.isHasLegalProblems()){
                                            ((EditText) rootView.findViewById(R.id.input_has_legal_problems_observation)).setText(person.getDescriptionLegalProblems());
                                        }

                                        // buscar datos como alumno
                                        searchStudentData(person.getId());
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {

                                }
                            });

                        } catch (Exception e) {
                            new MaterialDialog.Builder(getActivity())
                                    .title("Error")
                                    .content("Posible error al mostrar los datos escaneados. Por favor verifique si son correctos. Caso contrario intentelo nuevamente.")
                                    .positiveText("Aceptar")
                                    .onPositive((d, which) -> d.dismiss())
                                    .show();
                        }
                    } else {
                        new MaterialDialog.Builder(getActivity())
                                .title("Error")
                                .content("Hubo un error al escanear los datos. Intentelo nuevamente.")
                                .positiveText("Aceptar")
                                .onPositive((d, which) -> d.dismiss())
                                .show();
                    }
                    break;
            }
        }
    }

    private void searchStudentData(String personId) {
        studentViewModel.getStudent(personId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new MaybeObserver<StudentData>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(StudentData studentData) {
                // success
                ((EditText) rootView.findViewById(R.id.input_school_origin)).setText(studentData.SchoolOrigin);
                ((EditText) rootView.findViewById(R.id.input_another_phone_number_owner)).setText(studentData.OtherPhoneBelongs);
                ((EditText) rootView.findViewById(R.id.input_observation)).setText(studentData.Observation);

                int hb = studentData.HasBrothersOfSchoolAge ? 0 : 1;
                ((SwitchMultiButton) rootView.findViewById(R.id.has_brother_switch)).setSelectedTab(hb);

                int selectedCourse = 0;
                int selectedDivision = 0;
                for (CustomCourseYear cy : courseYearsList) {
                    CustomDivision division = stream(cy.divisions).where(e -> e.Id == studentData.DivisionId).firstOrNull();
                    if (division != null) {
                        selectedCourse = courseYearsList.indexOf(cy);
                        selectedDivision = cy.divisions.indexOf(division);
                        break;
                    }
                }

                ((SwitchMultiButton) rootView.findViewById(R.id.curso_switch)).setSelectedTab(selectedCourse);
                ((SwitchMultiButton) rootView.findViewById(R.id.division_switch)).setSelectedTab(selectedDivision);
                calcularEdad();
                // load documentations
                studentDocumentationViewModel.getDocumentations(personId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new MaybeObserver<List<Integer>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<Integer> documentations) {
                        // success
                        studentData.documentations = documentations;
                        finishLoadEvent("");
                        LinearLayout documentationsContainer = rootView.findViewById(R.id.documentations_container);
                        for(int documentation: studentData.documentations){
                            View v = documentationsContainer.findViewWithTag(documentation);
                            if(v instanceof CheckBox){
                                CheckBox checkBox = (CheckBox)v;
                                checkBox.setChecked(true);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.scan: {
                callScanner();
                break;
            }
        }
    }

    public void callScanner() {
        Intent i = new Intent(getActivity(), SimpleScannerActivity.class);
        startActivityForResult(i, SCANNER_ACTIVITY_RESULT);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        dateTextView.setText(simpleDateFormat.format(calendar.getTime()));
        calcularEdad();
    }

    void showDate(int year, int monthOfYear, int dayOfMonth, int spinnerTheme) {
        String bd = ((EditText) rootView.findViewById(R.id.input_birthdate)).getText().toString().trim();
        Date date;
        if (bd.isEmpty()) {
            date = new Date();
        } else {
            try {
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                date = format.parse(bd);
            } catch (Exception ignored) {
                date = new Date();
            }
        }
        new SpinnerDatePickerDialogBuilder()
                .context(getActivity())
                .callback(NewStudentFragment.this)
                .spinnerTheme(spinnerTheme)
                .defaultDate(Integer.parseInt(DateFormat.format("yyyy", date).toString()), Integer.parseInt(DateFormat.format("M", date).toString()) - 1, Integer.parseInt(DateFormat.format("d", date).toString()))
                .build()
                .show();
    }

    public StudentData getData() {
        StudentData studentData = new StudentData();
        studentData.setId(studentId);
        studentData.setFirstName(((EditText) rootView.findViewById(R.id.input_firstame)).getText().toString());
        studentData.setLastName(((EditText) rootView.findViewById(R.id.input_lastname)).getText().toString());
        studentData.setDocumentNumber(((EditText) rootView.findViewById(R.id.input_document_number)).getText().toString());

        int posGender = ((SwitchMultiButton) rootView.findViewById(R.id.gender)).getSelectedTab();
        if (posGender != -1) {
            PersonGenders personGenders = personGendersList.get(posGender);
            studentData.setPersonGenderId(personGenders.getId());
        } else {
            studentData.setPersonGenderId(posGender);
        }

        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            String bd = ((EditText) rootView.findViewById(R.id.input_birthdate)).getText().toString();
            studentData.setBirthdate(format.parse(bd));
        } catch (Exception e) {
        }

        studentData.setPhoneNumber(((EditText) rootView.findViewById(R.id.input_phone_number)).getText().toString());
        studentData.setMobilePhoneNumber(((EditText) rootView.findViewById(R.id.input_mobile_phone_number)).getText().toString());
        studentData.setAnotherContactPhone(((EditText) rootView.findViewById(R.id.input_another_phone_number)).getText().toString());
        studentData.setAddress(((EditText) rootView.findViewById(R.id.input_address)).getText().toString());
        studentData.setLocation(((EditText) rootView.findViewById(R.id.input_location)).getText().toString());
        studentData.setNationality(((EditText) rootView.findViewById(R.id.input_nacionality)).getText().toString());

        studentData.SchoolOrigin = ((EditText) rootView.findViewById(R.id.input_school_origin)).getText().toString();
        studentData.OtherPhoneBelongs = ((EditText) rootView.findViewById(R.id.input_another_phone_number_owner)).getText().toString();
        studentData.Observation = ((EditText) rootView.findViewById(R.id.input_observation)).getText().toString();
        studentData.documentations.clear();

        LinearLayout documentationsContainer = rootView.findViewById(R.id.documentations_container);
        for(int i = 0; i < documentationsContainer.getChildCount(); i++){
            CheckBox checkBox = (CheckBox) documentationsContainer.getChildAt(i);
            int documenation = (int)checkBox.getTag();
            if(!studentData.documentations.contains(documenation) && checkBox.isChecked()) {
                studentData.documentations.add(documenation);
            }
        }

        int pos = ((SwitchMultiButton) rootView.findViewById(R.id.curso_switch)).getSelectedTab();
        if (pos != -1) {
            CustomCourseYear c = courseYearsList.get(pos);
            if (divisionsString.length > 1) {
                int pd = ((SwitchMultiButton) rootView.findViewById(R.id.division_switch)).getSelectedTab();
                if (pd != -1) {
                    CustomDivision division = c.divisions.get(pd);
                    studentData.DivisionId = division.Id;
                } else {
                    studentData.DivisionId = pd;
                }
            } else {
                CustomDivision division = c.divisions.get(0);
                studentData.DivisionId = division.Id;
            }
        } else {
            studentData.DivisionId = pos;
        }

        studentData.IdCardBack = idCardBack;
        studentData.IdCardFront = idCardFront;
        int hasBrother = ((SwitchMultiButton) rootView.findViewById(R.id.has_brother_switch)).getSelectedTab();
        if (hasBrother != -1) {
            studentData.HasBrothersOfSchoolAge = hasBrother == 0;
            studentData.HasBrothersOfSchoolAgeAnswered = true;
        } else {
            studentData.HasBrothersOfSchoolAgeAnswered = false;
        }

        int hasEthnic = ((SwitchMultiButton) rootView.findViewById(R.id.belongs_ethnic_group)).getSelectedTab();
        if (hasEthnic != -1) {
            studentData.setBelongsEthnicGroup(hasEthnic == 0);
            studentData.BelongsEthnicGroupAnswered = true;
            if(studentData.isBelongsEthnicGroup()){
                studentData.setEthnicName(((EditText) rootView.findViewById(R.id.input_belongs_ethnic_group_observation)).getText().toString());
            }else{
                studentData.setEthnicName("");
            }
        } else {
            studentData.BelongsEthnicGroupAnswered = false;
            studentData.setEthnicName("");
        }

        int hasHealth = ((SwitchMultiButton) rootView.findViewById(R.id.has_health_problems)).getSelectedTab();
        if (hasHealth != -1) {
            studentData.setHasHealthProblems(hasHealth == 0);
            studentData.HasHealthProblemsAnswered = true;
            if(studentData.isHasHealthProblems()){
                studentData.setDescriptionHealthProblems(((EditText) rootView.findViewById(R.id.input_has_health_problems_observation)).getText().toString());
            }else{
                studentData.setDescriptionHealthProblems("");
            }
        } else {
            studentData.HasHealthProblemsAnswered = false;
            studentData.setDescriptionHealthProblems("");
        }

        int hasLegal = ((SwitchMultiButton) rootView.findViewById(R.id.has_legal_problems)).getSelectedTab();
        if (hasLegal != -1) {
            studentData.setHasLegalProblems(hasLegal == 0);
            studentData.HasLegalProblemsAnswered = true;
            if(studentData.isHasLegalProblems()){
                studentData.setDescriptionLegalProblems(((EditText) rootView.findViewById(R.id.input_has_legal_problems_observation)).getText().toString());
            }else{
                studentData.setDescriptionLegalProblems("");
            }
        } else {
            studentData.HasLegalProblemsAnswered = false;
            studentData.setDescriptionLegalProblems("");
        }

        studentData.setScannerResult(this.scannerResult);
        return studentData;
    }

    public void clearComponents() {
        studentId = null;
        ((EditText) rootView.findViewById(R.id.input_firstame)).setText("");
        ((EditText) rootView.findViewById(R.id.input_lastname)).setText("");
        ((EditText) rootView.findViewById(R.id.input_document_number)).setText("");
        ((SwitchMultiButton) rootView.findViewById(R.id.gender)).clearSelection();
        ((EditText) rootView.findViewById(R.id.input_birthdate)).setText("");
        ((EditText) rootView.findViewById(R.id.input_phone_number)).setText("");
        ((EditText) rootView.findViewById(R.id.input_mobile_phone_number)).setText("");
        ((EditText) rootView.findViewById(R.id.input_another_phone_number)).setText("");
        ((EditText) rootView.findViewById(R.id.input_address)).setText("");
        ((EditText) rootView.findViewById(R.id.input_location)).setText("");
        ((EditText) rootView.findViewById(R.id.input_school_origin)).setText("");
        ((EditText) rootView.findViewById(R.id.input_another_phone_number_owner)).setText("");
        ((EditText) rootView.findViewById(R.id.input_observation)).setText("");
        ((EditText) rootView.findViewById(R.id.input_nacionality)).setText("");
        ((SwitchMultiButton) rootView.findViewById(R.id.curso_switch)).clearSelection();
        ((SwitchMultiButton) rootView.findViewById(R.id.has_brother_switch)).clearSelection();
        ((SwitchMultiButton) rootView.findViewById(R.id.belongs_ethnic_group)).clearSelection();
        ((SwitchMultiButton) rootView.findViewById(R.id.has_health_problems)).clearSelection();
        ((SwitchMultiButton) rootView.findViewById(R.id.has_legal_problems)).clearSelection();
        rootView.findViewById(R.id.unique_division).setVisibility(View.GONE);
        rootView.findViewById(R.id.division_switch).setVisibility(View.GONE);
        ((SwitchMultiButton) rootView.findViewById(R.id.division_switch)).clearSelection();

        LinearLayout documentationsContainer = rootView.findViewById(R.id.documentations_container);
        for(int i = 0; i < documentationsContainer.getChildCount(); i++){
            CheckBox checkBox = (CheckBox) documentationsContainer.getChildAt(i);
            checkBox.setChecked(false);
        }

        idCardBack = "";
        idCardFront = "";
        new ClearImagen(rootView.findViewById(R.id.img_anverso_dni)).execute();
        new ClearImagen(rootView.findViewById(R.id.img_reverso_dni)).execute();
    }

    public void scrollTop() {
        ((ScrollView) rootView.findViewById(R.id.scroll_student)).fullScroll(ScrollView.FOCUS_UP);
    }


    private class LoadImagen extends AsyncTask<String, Integer, Boolean> {
        public String path;
        private ImageView imgView;

        public LoadImagen(String _path, ImageView _imgView) {
            path = _path;
            imgView = _imgView;
        }

        @Override
        protected Boolean doInBackground(String... params) {
            return true;
        }

        protected void onPostExecute(Boolean result) {
            if (path != null && !path.isEmpty()) {
                Glide.with(getActivity())
                        .load(path)
                        .centerCrop()
                        .error(R.drawable.ic_launcher_background)
                        .placeholder(R.drawable.loading)
                        .into(imgView);
                System.gc();
            }
        }

        @Override
        protected void onPreExecute() {

        }
    }

    private class ClearImagen extends AsyncTask<String, Integer, Boolean> {
        private ImageView imgView;

        public ClearImagen(ImageView _imgView) {
            imgView = _imgView;
        }

        @Override
        protected Boolean doInBackground(String... params) {
            return true;
        }

        protected void onPostExecute(Boolean result) {
            Glide.with(getActivity())
                    .load(R.drawable.ic_launcher_background)
                    .centerCrop()
                    .error(R.drawable.ic_launcher_background)
                    .placeholder(R.drawable.loading)
                    .into(imgView);
            System.gc();
        }

        @Override
        protected void onPreExecute() {

        }
    }

    private class CustomCourseYear {
        public int Id;
        public String CourseYearName;

        public CustomCourseYear(int Id, String courseYearName) {
            this.Id = Id;
            this.CourseYearName = courseYearName;
        }

        public List<CustomDivision> divisions = new ArrayList<>();
    }

    private class CustomDivision {
        public int Id;
        public String DivisionName;

        public CustomDivision(int Id, String divisionName) {
            this.Id = Id;
            this.DivisionName = divisionName;
        }
    }
}