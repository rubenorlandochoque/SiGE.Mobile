package com.smartapps.gestionescolar.ui.login;

import android.Manifest;
import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.smartapps.gestionescolar.R;
import com.smartapps.gestionescolar.classes.SharedConfig;
import com.smartapps.gestionescolar.models.courseyear.CourseYearViewModel;
import com.smartapps.gestionescolar.models.division.DivisionViewModel;
import com.smartapps.gestionescolar.models.documentations.DocumentationViewModel;
import com.smartapps.gestionescolar.models.persongenders.PersonGendersViewModel;
import com.smartapps.gestionescolar.models.relationship.RelationshipViewModel;
import com.smartapps.gestionescolar.models.shifts.ShiftViewModel;
import com.smartapps.gestionescolar.models.typeemployee.TypeEmployeeViewModel;
import com.smartapps.gestionescolar.services.AuthService;
import com.smartapps.gestionescolar.ui.shift.ShiftActivity;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.json.JSONObject;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function7;
import io.reactivex.schedulers.Schedulers;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private CourseYearViewModel courseYearViewModel;
    private DivisionViewModel divisionViewModel;
    private RelationshipViewModel relationshipViewModel;
    private PersonGendersViewModel personGendersViewModel;
    private ShiftViewModel shiftViewModel;
    private TypeEmployeeViewModel typeEmployeeViewModel;
    private DocumentationViewModel documentationViewModel;


    private ProgressDialog dialog;
    private TextInputEditText user;
    private TextInputEditText pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        courseYearViewModel = ViewModelProviders.of(this).get(CourseYearViewModel.class);
        divisionViewModel = ViewModelProviders.of(this).get(DivisionViewModel.class);
        relationshipViewModel = ViewModelProviders.of(this).get(RelationshipViewModel.class);
        personGendersViewModel = ViewModelProviders.of(this).get(PersonGendersViewModel.class);
        shiftViewModel = ViewModelProviders.of(this).get(ShiftViewModel.class);
        typeEmployeeViewModel = ViewModelProviders.of(this).get(TypeEmployeeViewModel.class);
        documentationViewModel = ViewModelProviders.of(this).get(DocumentationViewModel.class);

        user = findViewById(R.id.input_user_name);
        pass = findViewById(R.id.input_password);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        findViewById(R.id.signinBtn).setOnClickListener(this);
        checkPermissions();

        findViewById(R.id.styledBackground).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                LayoutInflater inflater = LoginActivity.this.getLayoutInflater();
                View root = inflater.inflate(R.layout.dialog_url, null);
                ((EditText) root.findViewById(R.id.url_server)).setText(SharedConfig.getServerUrl());
                builder.setView(root)
                        .setPositiveButton("Aceptar", (dialog, id) -> {
                            SharedConfig.setServerUrl(((EditText) root.findViewById(R.id.url_server)).getText().toString());
                        })
                        .setNegativeButton("Cancelar", (dialog, id) -> dialog.cancel());
                builder.create().show();
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signinBtn:
                if (isValid()) {
                    dialog = new ProgressDialog(this, R.style.CustomDialogStyle);
                    dialog.setTitle("");
                    dialog.setMessage("Validando usuario.");
                    dialog.setIndeterminate(true);
                    dialog.show();
                    new AuthService().login(user.getText().toString(), pass.getText().toString(), new AuthService.OnLoginEvent() {
                        @Override
                        public void successLogin() {
                            if (dialog != null) {
                                dialog.dismiss();
                            }
                            dialog = new ProgressDialog(LoginActivity.this, R.style.CustomDialogStyle);
                            dialog.setTitle("");
                            dialog.setMessage("Descargando datos.");
                            dialog.setIndeterminate(true);
                            dialog.show();
                            Observable.zip(courseYearViewModel.rxDownload(), divisionViewModel.rxDownload(), relationshipViewModel.rxDownload(), personGendersViewModel.rxDownload(), shiftViewModel.rxDownload(), typeEmployeeViewModel.rxDownload(), documentationViewModel.rxDownload(),
                                    new Function7<JSONObject, JSONObject, JSONObject, JSONObject, JSONObject, JSONObject, JSONObject, Result>() {
                                        @Override
                                        public Result apply(JSONObject cy, JSONObject d, JSONObject r, JSONObject pg, JSONObject s, JSONObject te, JSONObject doc) throws Exception {
                                            Result re = new Result();
                                            re.divisions = d;
                                            re.courseYears = cy;
                                            re.personGenders = pg;
                                            re.relationships = r;
                                            re.shift = s;
                                            re.typeEmployees = te;
                                            re.documentations = doc;

                                            return re;
                                        }
                                    })
                                    .subscribeOn(Schedulers.newThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new io.reactivex.Observer<Result>() {
                                        @Override
                                        public void onSubscribe(Disposable d) {

                                        }

                                        @Override
                                        public void onNext(Result result) {
                                            Single<String> insert = Single.create(emitter -> {
                                                // Save data
                                                try {
                                                    courseYearViewModel.save(result.courseYears);
                                                    divisionViewModel.save(result.divisions);
                                                    relationshipViewModel.save(result.relationships);
                                                    personGendersViewModel.save(result.personGenders);
                                                    shiftViewModel.save(result.shift);
                                                    typeEmployeeViewModel.save(result.typeEmployees);
                                                    documentationViewModel.save(result.documentations);
                                                    emitter.onSuccess("true");
                                                } catch (Exception e) {
                                                    emitter.onError(e);
                                                }
                                            });
                                            insert.subscribeOn(Schedulers.newThread())
                                                    .observeOn(AndroidSchedulers.mainThread())
                                                    .subscribe(new SingleObserver<String>() {
                                                        @Override
                                                        public void onSubscribe(Disposable d) {

                                                        }

                                                        @Override
                                                        public void onSuccess(String s) {
                                                            if (dialog != null && dialog.isShowing()) {
                                                                dialog.dismiss();
                                                            }
                                                            Intent intent = new Intent(LoginActivity.this, ShiftActivity.class);
                                                            startActivity(intent);
                                                            finish();
                                                        }

                                                        @Override
                                                        public void onError(Throwable e) {
                                                            if (dialog != null && dialog.isShowing()) {
                                                                dialog.dismiss();
                                                            }
                                                        }
                                                    });

                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            if (dialog != null && dialog.isShowing()) {
                                                dialog.dismiss();
                                            }
                                            new MaterialDialog.Builder(LoginActivity.this)
                                                    .title("Error")
                                                    .content("Hubo un error al descargar los datos.\nPor favor intente nuevamente")
                                                    .positiveText("Aceptar")
                                                    .show();
                                        }

                                        @Override
                                        public void onComplete() {

                                        }
                                    });
                        }

                        @Override
                        public void errorLogin(String msg) {
                            if (dialog != null && dialog.isShowing()) {
                                dialog.dismiss();
                            }
                            new MaterialDialog.Builder(LoginActivity.this)
                                    .title("Error")
                                    .content("Hubo un error al iniciar sesión.\nPor favor intente nuevamente")
                                    .positiveText("Aceptar")
                                    .show();
                        }
                    });
                }
                break;
        }
    }

    private boolean isValid() {
        String txtUser = user.getText().toString();
        String txtPass = pass.getText().toString();
        if (txtUser.isEmpty()) {
            Toast.makeText(getApplicationContext(),
                    "Debe ingresar el usuario.",
                    Toast.LENGTH_LONG).show();
            return false;
        }
        if (txtPass.isEmpty()) {
            Toast.makeText(getApplicationContext(),
                    "Debe ingresar la contraseña.",
                    Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void checkPermissions() {
        RxPermissions rxPermissions = new RxPermissions(this);
        Disposable subscribe = rxPermissions
                .request(Manifest.permission.CAMERA, Manifest.permission.INTERNET, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(granted -> {
                });
    }

    private class Result {
        public JSONObject divisions;
        public JSONObject courseYears;
        public JSONObject personGenders;
        public JSONObject relationships;
        public JSONObject shift;
        public JSONObject typeEmployees;
        public JSONObject documentations;
    }
}
