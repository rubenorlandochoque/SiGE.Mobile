package com.smartapps.gestionescolar.ui.newstudent;

import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.smartapps.gestionescolar.R;
import com.smartapps.gestionescolar.models.students.StudentViewModel;
import com.smartapps.gestionescolar.models.students.pojo.StudentData;
import com.smartapps.gestionescolar.models.tutor.pojo.TutorData;
import com.smartapps.gestionescolar.util.Evento;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class StudentTutorActivity extends AppCompatActivity {
    private ViewPager mViewPager;
    private TabLayout tabLayout;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ProgressDialog dialog;
    private StudentViewModel studentViewModel;
    private String studentId;
    private String tutorId;
    private int courseYearId;
    private final CompositeDisposable mDisposable = new CompositeDisposable();
    int loaded = 0;
    private String tutorSaved = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        setContentView(R.layout.activity_student_tutor);

        if ((dialog == null || !dialog.isShowing())) {
            dialog = ProgressDialog.show(this, "",
                    "Cargando", true);
        }
        loaded = 0;

//        View models
        studentViewModel = ViewModelProviders.of(this).get(StudentViewModel.class);

        studentId = getIntent().getStringExtra("studentId");
        tutorId = getIntent().getStringExtra("tutorId");
        courseYearId = getIntent().getIntExtra("courseYearId", 0);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), studentId, tutorId, courseYearId);
        tabLayout = findViewById(R.id.tab_layout);

        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout.addTab(tabLayout.newTab().setText("Alumno"));
        tabLayout.addTab(tabLayout.newTab().setText("Tutor"));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tab.getPosition()) {
                    case 0:
                        mViewPager.setCurrentItem(0);
                        break;
                    case 1:
                        mViewPager.setCurrentItem(1);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (tabLayout != null) {
                    tabLayout.getTabAt(position).select();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        findViewById(R.id.sc).setOnClickListener(view -> {
            Fragment page = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.container + ":" + mViewPager.getCurrentItem());
            if (page instanceof NewStudentFragment) {
                ((NewStudentFragment) page).callScanner();
            }

            if (page instanceof NewTutorFragment) {
                ((NewTutorFragment) page).callScanner();
            }
            Log.e("", "");
        });
    }

    public void save(View view) {
        Fragment s = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.container + ":0");
        Fragment t = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.container + ":1");
        if ((dialog == null || !dialog.isShowing())) {
            dialog = ProgressDialog.show(this, "",
                    "Guardando", true);
        }

        StudentData studentData = ((NewStudentFragment) s).getData();
        TutorData tutorData = ((NewTutorFragment) t).getData();

        String result = validar(studentData, tutorData);
        if (result.isEmpty()) {
            tutorSaved = "";
            studentViewModel.saveStudent(studentData, tutorData).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<String>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onSuccess(String tutor) {
                    tutorSaved = tutor;
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    new MaterialDialog.Builder(StudentTutorActivity.this)
                            .title("Guardado Correcto")
                            .content("Datos guardados correctamente")
                            .positiveText("Aceptar")
                            .onPositive((d, which) -> {
                                d.dismiss();
                                if (studentId == null || studentId.isEmpty()) {
                                    StudentTutorActivity.this.nuevoAlumnoMismoTutor();
                                } else {
                                    StudentTutorActivity.this.finish();
                                }
                            })
                            .show();
                }

                @Override
                public void onError(Throwable e) {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    new MaterialDialog.Builder(StudentTutorActivity.this)
                            .title("Error")
                            .content("Hubo un error al guardar los datos. Intente nuevamente. Si el error persiste contacte con el adminsitrador.")
                            .positiveText("Aceptar")
                            .onPositive((d, which) -> d.dismiss())
                            .show();
                }
            });
        } else {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
            new MaterialDialog.Builder(StudentTutorActivity.this)
                    .title("Error")
                    .content(result)
                    .positiveText("Aceptar")
                    .onPositive((d, which) -> d.dismiss())
                    .show();
        }
    }

    private void nuevoAlumnoMismoTutor() {
        new MaterialDialog.Builder(StudentTutorActivity.this)
                .title("")
                .content("¿Desea cargar un nuevo alumno con el mismo tutor?")
                .positiveText("Si")
                .negativeText("no")
                .onPositive((d, which) -> {
                    d.dismiss();
                    Fragment s = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.container + ":0");
                    ((NewStudentFragment) s).clearComponents();

                    Fragment t = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.container + ":1");
                    ((NewTutorFragment) t).setTutorId(tutorSaved);

                    mViewPager.setCurrentItem(0);
                    ((NewStudentFragment) s).scrollTop();
                })
                .onNegative((dialog, which) -> {
                    dialog.dismiss();
                    StudentTutorActivity.this.finish();
                })
                .show();
    }

    private String validar(StudentData studentData, TutorData tutorData) {
        if (studentData.getFirstName().isEmpty()) {
            return "Debe ingresar nombre del alumno";
        }
        if (studentData.getLastName().isEmpty()) {
            return "Debe ingresar apellido del alumno";
        }
        if (studentData.getDocumentNumber().isEmpty()) {
            return "Debe ingresar número de documento del alumno";
        }

        if (studentData.getPersonGenderId() == -1) {
            return "Debe ingresar sexo del alumno";
        }

        if (studentData.getBirthdate() == null) {
            return "Debe ingresar fecha de nacimiento del alumno";
        }

        if (!studentData.HasBrothersOfSchoolAgeAnswered) {
            return "Debe ingresar si el alumno tiene hermanos en edad escolar";
        }

        if (studentData.DivisionId == -1) {
            return "Debe ingresar año y division del alumno";
        }

        if (tutorData.getFirstName().isEmpty()) {
            return "Debe ingresar nombre del tutor";
        }
        if (tutorData.getLastName().isEmpty()) {
            return "Debe ingresar apellido del tutor";
        }
        if (tutorData.getDocumentNumber().isEmpty()) {
            return "Debe ingresar número de documento del tutor";
        }
        if (tutorData.getPersonGenderId() == -1) {
            return "Debe ingresar sexo del tutor";
        }
        if (tutorData.getBirthdate() == null) {
            return "Debe ingresar fecha de nacimiento del tutor";
        }

        if (tutorData.getRelationshipId() == -1) {
            return "Debe ingresar el parentesco del tutor";
        }

        return "";
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventListener(Evento event) {
        switch (event.eventName) {
            case "finishLoadData":
                loaded++;
                if (event.data.toString().isEmpty()) {
                    if (loaded == 2) {
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    }
                } else {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    new MaterialDialog.Builder(StudentTutorActivity.this)
                            .title("Error")
                            .content(event.data.toString())
                            .positiveText("Aceptar")
                            .onPositive((d, which) -> {
                                d.dismiss();
                                finish();
                            })
                            .show();
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        mDisposable.clear();
    }

    @Override
    public void onBackPressed() {
        new MaterialDialog.Builder(StudentTutorActivity.this)
                .title("Info")
                .content("¿Salir sin guardar los cambios?")
                .positiveText("Si")
                .negativeText("No")
                .onPositive((d, which) -> {
//                    Fragment s = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.container + ":0");
//                    StudentData studentData = ((NewStudentFragment) s).getData();
                    d.dismiss();
                    finish();
                })
                .onNegative((d, which) -> d.dismiss())
                .show();
    }
}
