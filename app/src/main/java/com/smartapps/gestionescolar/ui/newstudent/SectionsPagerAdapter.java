package com.smartapps.gestionescolar.ui.newstudent;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    String studentId;
    String tutorId;
    int courseYearId;

    public SectionsPagerAdapter(FragmentManager supportFragmentManager, String studentId, String tutorId, int courseYearId) {
        super(supportFragmentManager);
        this.studentId = studentId;
        this.tutorId = tutorId;
        this.courseYearId = courseYearId;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a NewStudentFragment (defined as a static inner class below).
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new NewStudentFragment();
                Bundle bundle = new Bundle();
                bundle.putString("studentId", studentId);
                bundle.putInt("courseYearId", courseYearId);
                fragment.setArguments(bundle);
                break;
            case 1:
                fragment = new NewTutorFragment();
                bundle = new Bundle();
                bundle.putString("tutorId", tutorId);
                bundle.putString("studentId", studentId);
                fragment.setArguments(bundle);
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "SECTION 1";
        }
        return null;
    }
}