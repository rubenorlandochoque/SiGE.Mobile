package com.smartapps.gestionescolar.ui.main;

import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.smartapps.gestionescolar.R;
import com.smartapps.gestionescolar.classes.SharedConfig;
import com.smartapps.gestionescolar.models.students.StudentViewModel;
import com.smartapps.gestionescolar.models.teacher.TeacherViewModel;
import com.smartapps.gestionescolar.services.Upload;
import com.smartapps.gestionescolar.ui.login.LoginActivity;
import com.smartapps.gestionescolar.ui.multimedia.RoundedImageView;
import com.smartapps.gestionescolar.ui.shift.ShiftActivity;
import com.smartapps.gestionescolar.ui.summary.CourseYearListActivity;
import com.smartapps.gestionescolar.ui.summaryteacher.TeacherListActivity;

public class MainActivity extends AppCompatActivity {
    private ProgressDialog dialog;
    private StudentViewModel studentViewModel;
    private TeacherViewModel teacherViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        studentViewModel = ViewModelProviders.of(this).get(StudentViewModel.class);
        teacherViewModel = ViewModelProviders.of(this).get(TeacherViewModel.class);
        setDrawerMenu();
    }

    public void onStudentsClick(View view) {
        Intent intent = new Intent(this, CourseYearListActivity.class);
        startActivity(intent);
    }

    public void onTeachersClick(View view) {
        Intent intent = new Intent(this, TeacherListActivity.class);
        startActivity(intent);
    }


    private void setDrawerMenu() {
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navView = findViewById(R.id.navview);

        findViewById(R.id.btn_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        View headerView = navView.getHeaderView(0);
        RoundedImageView imgUser = headerView.findViewById(R.id.img_user_photo);
        TextView txtUser = headerView.findViewById(R.id.txt_user_name);

        if (SharedConfig.getUserShiftIds().size() > 1) {
            navView.getMenu().findItem(R.id.menu_change_shift).setVisible(true);
        } else {
            navView.getMenu().findItem(R.id.menu_change_shift).setVisible(false);
        }

        //txtUser.setText(SharedConfig.getUserName() + " " + SharedConfig.getUserSurname());
        txtUser.setText("user");
//        Glide.with(this)
//                .load(SharedConfig.getUserPhoto())
//                .asBitmap()
//                .placeholder(R.drawable.default_user)
//                .centerCrop()
//                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                .into(imgUser);

        navView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.menu_loguot:
                                closeSession();
                                break;
                            case R.id.menu_sync:
                                if ((dialog == null || !dialog.isShowing())) {
                                    dialog = ProgressDialog.show(MainActivity.this, "",
                                            "" +
                                                    "Subiendo datos", true);
                                }
                                new Upload().Sync(studentViewModel, teacherViewModel, new Upload.UploadListner() {
                                    @Override
                                    public void finish(boolean successImages, boolean successStudent, boolean successTeacher) {
                                        if (dialog != null && dialog.isShowing()) {
                                            dialog.dismiss();
                                        }

                                        if(successImages && successStudent && successTeacher) {
                                            new MaterialDialog.Builder(MainActivity.this)
                                                    .title("Sincronización completa")
                                                    .content("Los datos se sincronizaron correctamente")
                                                    .positiveText("Aceptar")
                                                    .onPositive((d, which) -> {
                                                        d.dismiss();
                                                    })
                                                    .show();
                                        }else{
                                            new MaterialDialog.Builder(MainActivity.this)
                                                    .title("Error al sincronizar")
                                                    .content("Hubo un error al sincronizar los datos.\nPor favor intente nuevamente.")
                                                    .positiveText("Aceptar")
                                                    .onPositive((d, which) -> {
                                                        d.dismiss();
                                                    })
                                                    .show();
                                        }
                                    }
                                });
                                break;
                            case R.id.menu_change_shift:
                                Intent intent = new Intent(MainActivity.this, ShiftActivity.class);
                                intent.putExtra("origin", "MainActivity");
                                startActivity(intent);
                                break;
                        }

                        drawerLayout.closeDrawers();

                        return true;
                    }
                });
    }

    @Override
    public void onBackPressed() {
        new MaterialDialog.Builder(MainActivity.this)
                .title("Info")
                .content("¿Desea cerrar la aplicación?")
                .positiveText("Si")
                .negativeText("No")
                .onPositive((d, which) -> {
                    d.dismiss();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        super.finishAndRemoveTask();
                    } else {
                        super.finish();
                    }
                })
                .onNegative((d, which) -> d.dismiss())
                .show();
    }

    private void closeSession() {
        new MaterialDialog.Builder(MainActivity.this)
                .title("Info")
                .content("¿Cerrar sesión?")
                .positiveText("Si")
                .negativeText("No")
                .onPositive((d, which) -> {
                    d.dismiss();
                    SharedConfig.setToken("");
                    SharedConfig.setUserCode("");
                    SharedConfig.setUserName("");
                    SharedConfig.setUserPhoto("");
                    SharedConfig.setUserSurname("");
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                })
                .onNegative((d, which) -> d.dismiss())
                .show();
    }

    @Override
    public void onResume() {
        super.onResume();
        String shit = SharedConfig.getShiftDescription();
        ((TextView) findViewById(R.id.txt_ab_title)).setText(String.format("Turno: %s", shit));
    }
}
