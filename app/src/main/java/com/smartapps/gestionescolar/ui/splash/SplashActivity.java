package com.smartapps.gestionescolar.ui.splash;

import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.smartapps.gestionescolar.BuildConfig;
import com.smartapps.gestionescolar.R;
import com.smartapps.gestionescolar.classes.SharedConfig;
import com.smartapps.gestionescolar.models.base.BaseViewModel;
import com.smartapps.gestionescolar.models.courseyear.CourseYear;
import com.smartapps.gestionescolar.models.courseyear.CourseYearViewModel;
import com.smartapps.gestionescolar.ui.login.LoginActivity;
import com.smartapps.gestionescolar.ui.shift.ShiftActivity;
import com.smartapps.gestionescolar.util.Util;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class SplashActivity extends AppCompatActivity {
    private ProgressDialog dialog;
    private BaseViewModel baseViewModel;
    private CourseYearViewModel courseYearViewModel;
    private final CompositeDisposable mDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (SharedConfig.getServerUrl().isEmpty()) {
            SharedConfig.setServerUrl(BuildConfig.SERVER_URL);
        }
        baseViewModel = ViewModelProviders.of(this).get(BaseViewModel.class);
        courseYearViewModel = ViewModelProviders.of(this).get(CourseYearViewModel.class);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if ((dialog == null || !dialog.isShowing())) {
            dialog = ProgressDialog.show(this, "",
                    "Cargando", true);
        }
//        mDisposable.add(baseViewModel.seed()
//                .subscribeOn(Schedulers.io())
//                .observeOn(Schedulers.io())
//                .subscribe(() ->
//                        {
//                            if ((dialog != null && dialog.isShowing())) {
//                                dialog.dismiss();
//                            }
//
//                            new Task<String>() {
//                                @Override
//                                public String task() {
//                                    try {
//                                        Thread.sleep(1000);
//                                    } catch (InterruptedException e) {
//                                        Log.e("SplashActivity",e.getMessage());
//                                    }
//                                    valid();
//                                    return null;
//                                }
//                            }.run();
////                            Intent i = new Intent(SplashActivity.this, LoginActivity.class);
////                            startActivity(i);
////                            finish();
//                        },
//                        throwable -> {
//                            if (dialog != null && dialog.isShowing()) {
//                                dialog.dismiss();
//                            }
//                        }));
        courseYearViewModel.getAllSingle()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<CourseYear>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<CourseYear> courseYears) {
                        if (courseYears.size() == 0) {
                            SharedConfig.setToken("");
                            SharedConfig.setUserCode("");
                            SharedConfig.setUserName("");
                            SharedConfig.setUserPhoto("");
                            SharedConfig.setUserSurname("");
                        }
                        valid();
                    }

                    @Override
                    public void onError(Throwable e) {
                        SharedConfig.setToken("");
                        SharedConfig.setUserCode("");
                        SharedConfig.setUserName("");
                        SharedConfig.setUserPhoto("");
                        SharedConfig.setUserSurname("");
                        valid();
                    }
                });
    }


    private void valid() {
        if ((dialog != null && dialog.isShowing())) {
            dialog.dismiss();
        }
        if (SharedConfig.getToken() == null || SharedConfig.getToken().isEmpty()) {
            if (!Util.isNetworkAvailable()) {
                runOnUiThread(new Thread(new Runnable() {
                    public void run() {
                        new MaterialDialog.Builder(SplashActivity.this)
                                .title("Aviso")
                                .content(R.string.notinternet)
                                .positiveText("Aceptar").onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                SplashActivity.this.finish();
                            }
                        }).show();
                    }
                }));
                return;
            }
            Intent i = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        } else {
            Intent i = new Intent(SplashActivity.this, ShiftActivity.class);
            startActivity(i);
            finish();
        }
//        if(NetworkUtil.getConnectivityStatusString()){
//            new Thread(){
//                public void run() {
//                    PendienteService.getInstance().uploadPendientes();
//                }
//            }.run();
//            new Thread(){
//                public void run() {
//                    FileSyncService.getInstance().uploadFiles();
//                }
//            }.run();
//        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // clear all the subscriptions
        mDisposable.clear();
    }
}
