package com.smartapps.gestionescolar.ui.summaryteacher;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smartapps.gestionescolar.R;
import com.smartapps.gestionescolar.models.teacher.pojo.TeacherListData;
import com.smartapps.gestionescolar.ui.teacher.TeacherActivity;

import java.util.List;

public class TeacherListAdapter extends RecyclerView.Adapter<TeacherListAdapter.StudentsListHolder> {

    private final Context context;

    class StudentsListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView courseYear;
        private final TextView inscriptos;

        TeacherListAdapter.ItemClickListener onItemClickListener;

        private StudentsListHolder(View itemView) {
            super(itemView);
            courseYear = itemView.findViewById(R.id.textView);
            inscriptos = itemView.findViewById(R.id.inscriptos);
            itemView.setOnClickListener(this);
        }

        public void setOnItemClickListener(TeacherListAdapter.ItemClickListener onItemClickListener) {
            this.onItemClickListener = onItemClickListener;
        }

        @Override
        public void onClick(View view) {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(view, getAdapterPosition());
            }
        }
    }

    private final LayoutInflater mInflater;
    private List<TeacherListData> teacherListData;

    public TeacherListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public TeacherListAdapter.StudentsListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new TeacherListAdapter.StudentsListHolder(itemView);
    }

    @Override
    public void onBindViewHolder(StudentsListHolder holder, int position) {
        TeacherListData current = teacherListData.get(position);
        holder.courseYear.setText(String.format("%s, %s", current.LastName, current.FirstName));
        holder.inscriptos.setText(current.DocumentNumber);
        holder.setOnItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                TeacherListData selected = teacherListData.get(position);
                Intent intent = new Intent(context, TeacherActivity.class);
                intent.putExtra("teacherId", selected.Id);
                context.startActivity(intent);
            }
        });
    }

    public void setTeacherListData(List<TeacherListData> teacherListData) {
        this.teacherListData = teacherListData;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (teacherListData != null)
            return teacherListData.size();
        else return 0;
    }

    interface ItemClickListener {
        void onClick(View view, int position);
    }
}


