package com.smartapps.gestionescolar;

import android.app.Application;
import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.facebook.stetho.Stetho;

public class App extends Application {
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        AndroidNetworking.initialize(getApplicationContext());
        Stetho.initializeWithDefaults(this);
    }

    public static Context getAppContext() {
        return context;
    }
}